
\paragraphtitle{¿RISC o CISC?}
Primeramente, es útil considerar si la arquitectura a diseñar será de tipo RISC
o CISC. Aunque el número de operaciones pueda ser pequeño en cualquiera de los dos
casos, una arquitectura CISC tendría operaciones complejas que dificultarían el
diseño de la unidad de control y de la ruta de datos. En el ejemplo dado en la
sección anterior, la instrucción
CISC \textit{MULT} leía de memoria, multiplicaba y luego escribía en memoria el
resultado, a comparación de una arquitectura RISC que ejecutaba cuatro
instrucciones para realizar lo mismo. A día de hoy, el 99\%~\cite{PattersonTuring}
de las computadoras
utilizan arquitecturas RISC, debido a las ventajas expuestas anteriormente.
Aunque algunas se declaren CISC, como x86, o sean claramente CISC a ojos del
programador, internamente a veces utilizan conversiones a microcódigo RISC~\cite{intelRISC}.
Realizar el diseño de una arquitectura RISC simplificará la realización de muchas
de sus partes y facilitará la comprensión, además de hacer la solución más sencilla
de comprender visualmente. 

\paragraphtitle{Codificación del repertorio de instrucciones:}
Según cómo se codifiquen las instrucciones de un repertorio, el tamaño de los programas será
mayor o menor. Además, esta codificación afectará directamente a cómo se realiza la
implementación. Existen tres alternativas:
\begin{itemize}
    \item \textbf{Longitud variable:}
        Las instrucciones podrán tener longitudes diferentes.
    \item \textbf{Longitud fija:}
        Todas las instrucciones tendrán la misma longitud.
\end{itemize}
Es habitual que los repertorios RISC tengan instrucciones de longitud fija, ya que facilita
el diseño y esto hace que se puedan optimizar mejor. Repertorios como x86 de estilo CISC
es habitual que contengan instrucciones de longitud diferentes.


\paragraphtitle{Tipo de almacenamiento de operandos}
El tipo de almacenamiento de los operandos se refiere a dónde vamos a mantener los
operandos con los que vamos a operar, es decir, sobre los cuales ejecutaremos las
instrucciones. Respecto a esta cuestión, existen tres aproximaciones principales:
\begin{itemize}
    \item \textbf{Pila:}
        Los operandos son implícitos, se encuentran en la parte superior de una
        pila que el sistema deberá proporcionar. Las instrucciones que utilicen
        este tipo de almacenamiento no necesitarán indicar dónde se encuentran
        los datos sobre los que va a operar. Para operar la pila se utilizarán
        instrucciones como \textit{POP} o \textit{PUSH}, y se tendrán registros
        especiales que señalarán la posición actual de la pila y la base. En la
        arquitectura \textbf{x86}, estos registros se denominan \textit{ESP} y
        \textit{EBP}, aunque en realidad está operando sobre una sección de la
        memoria principal a la que considera la pila, no sobre un componente
        hardware aislado e independiente.
    \item \textbf{Acumulador:}
        Consiste en tener un registro particular al que se denomina acumulador,
        y que almacenará los operandos de forma implícita, es decir, en las
        instrucciones que utilicen este tipo de almacenamiento no necesitarán
        indicar dónde se encuentra uno de los datos, porque estará en el
        acumulador. Siguiendo con el ejemplo de \textbf{x86}, aunque no utiliza
        un acumulador como tal, en algunas instrucciones como \textit{MUL} o
        \textit{DIV}, sí que utiliza los registros \textit{EAX} y \textit{EDX}
        de manera implícita, a modo de acumulador primario y secundario.
    \item \textbf{Registros de propósito general:}
        La arquitectura cuenta con varios registros en los que se pueden almacenar
        datos a petición. Los operandos se encuentran alojados en alguno de los
        registros disponibles, y para operar con ellos se indica el registro en el
        que se encuentra. Pueden ser dos operandos o tres, dependiendo de si se 
        especifican tanto los operandos como el registro que almacenará el resultado,
        o si simplemente se indican los operandos y el resultado se guarda en alguno
        de los registros que también guardaba un operando.
        Tanto \textbf{x86} como \textbf{x86\_64} cuentan con registros de propósito
        general: 8 en el caso de x86 y 16 en caso de x86\_64.
        Usando este tipo de almacenamiento podemos elegir otros factores:
        \begin{itemize}
            \item \textbf{Registro-Registro:}
                Todos los operandos deben estar en registros del procesador antes
                de operar con ellos. Una instrucción de la forma
                \textit{add eax, eax} entraría dentro de esta categoría.
            \item \textbf{Registro-Memoria:}
                Un operando debe estar en registro y el otro está en memoria. Por
                ejemplo: \textit{mul eax, [0x10]} que multiplicará lo que haya en
                el registro \textit{EAX} con lo que haya en la posición 0x10 de
                la memoria. 
            \item \textbf{Memoria-Memoria:}
                Todos los operandos se encuentran en memoria. Por ejemplo:
                \textit{add [eax], [edx]}, que suma lo que haya en memoria en la
                posición señalada por el contenido del registro \textit{EAX}, con lo que haya
                en memoria en la posición señalada por el contenido del registro
                \textit{EDX}.
        \end{itemize}
\end{itemize}

La utilización de arquitecturas basadas en registros de propósito general está
muy extendida, tanto en  arquitecturas de computadoras personales como x86 y
x86\_64 como para arquitecturas de microcontroladores como AVR o la familia Cortex-M
de ARM.


\paragraphtitle{Modos de direccionamiento:}
El modo de direccionamiento es la manera de especificar los operandos dentro de las
instrucciones. Independientemente del tipo de almacenamiento usado, podemos utilizar
varias formas para referirnos a la forma en la que vamos a especificar los operandos.
Hay varias opciones:
\begin{itemize}
    \item \textbf{Inmediato:}
        El operando se codifica dentro de la instrucción, de manera directa. Así, una
        instrucción sería \textit{PUSH 0x10} para introducir el valor 0x10 en la
        pila, o \textit{ADD 0x10, 0x20} para sumar 0x10 con 0x20.
    \item \textbf{Registro:}
        Se indica el registro en el que está almacenado el dato con el que queremos
        operar. Ejemplos de instrucciones serían \textit{PUSH EAX} o
        \textit{XOR EAX, EAX}.
    \item \textbf{Directo o absoluto:}
        Se indica la dirección de memoria en la que está almacenado el dato con el
        que queremos operar. \textit{PUSH [0xABC]}, o bien \textit{SUB [0xFF], [0x20]}.
    \item \textbf{Indirecto:}
        Se indica el registro en el que está almacenada la dirección de memoria en
        la que está el dato con el que queremos operar. La sintaxis es similar a la
        del modo por registro, solo que el registro no contiene el dato directamente,
        sino la dirección de memoria en la que se encuentra el dato.
    \item \textbf{Indirecto con desplazamiento:}
        Similar al modo anterior, pero además se incluye un operando inmediato: el
        desplazamiento u offset. Al sumar este desplazamiento a la dirección contenida
        en el registro indicado, obtendremos la dirección de memoria en la que se encuentra
        el dato con el que queremos operar.
        Puede considerarse el modo indirecto como que tiene un desplazamiento con valor 0.
\end{itemize}
Hay mucha variedad tanto en los diferentes modos de direccionamiento como en los nombres
con los que se nombra a cada uno. Hay arquitecturas como MIPS-X que tienen un solo modo
de direccionamiento~\cite{Hennessy1998},
mientras otras como la AVR soporta hasta 15 modos diferentes~\cite{AVRISA}.
Implementar modos de direccionamiento complejos puede aumentar la complejidad del hardware.
Es interesante el concepto de ortogonalidad en el juego de instrucciones. Este concepto
hace referencia a la capacidad de que cada instrucción pueda utilizar cualquiera de los
modos de direccionamiento implementados en la arquitectura. Aunque los juegos de instrucciones
a día de hoy no se diseñan teniendo la ortogonalidad en mente, sí que traen algunos beneficios
como poder definir aspectos suyos de manera aislada, sin afectar a los demás~\cite{ARMOrth}.


\paragraphtitle{Fases de ejecución de una instrucción:}
Es habitual dividir la ejecución de las instrucciones en fases o etapas. Tanto las arquitecturas
RISC como las CISC utilizan técnicas de segmentado de la ejecución, también llamado pipeline. 
En las arquitecturas RISC la
implementación del segmentado es más directa y sencilla, siendo cinco el número de fases en una
implementación clásica: Obtención de la instrucción, decodificación, ejecución, acceso a memoria
y escritura (Fetch, decode, execution, memory access, writeback, en inglés). La elección del
número de etapas es muy dependiente del contexto de la arquitectura. Muchas arquitecturas siguen
respondiendo al modelo clásico de cinco etapas, o haciendo modificaciones que añaden algunas.
Un ejemplo que sobresale es la arquitectura Prescott de Intel que llegó a tener un 31~\cite{Prescott}.
Sin embargo, otras, como algunas de AVR, funcionan con un pipeline de 3 etapas~\cite{ATMEL}.


\paragraphtitle{Monociclo vs. Multiciclo:}
Una arquitectura monociclo ejecutará una instrucción en cada ciclo, mientras que
una multiciclo tardará varios ciclos en ejecutarlas (se pueden dar dos casos:
que todas las instrucciones tarden los mismos ciclos en ejecutarse, o que
haya instrucciones que tarden más ciclos que otras).
Esta clasificación está relacionada con la anterior en el sentido de que originalmente
el segmentado se hizo con la intención de que cada fase tardase un ciclo en ejecutarse.
De esta manera, en un pipeline de \textit{n} fases, cada instrucción tardaría
\textit{n} ciclos en ejecutarse.

A día de hoy la inmensa mayoría de arquitecturas son multiciclo segmentadas. Esto
se debe entre otras cosas a que implementan instrucciones cuyos tiempos y
complejidades de ejecución son muy dispares.
Hay instrucciones cuya ejecución es casi inmediata, como por
ejemplo la instrucción \textit{MOV}, y también hay instrucciones cuya ejecución es
larga y tediosa, como \textit{DIV}. Para que ambas instrucciones se ejecuten en un solo ciclo,
ese ciclo tendría que tener la duración de la instrucción más larga, y se perdería
mucho tiempo en las instrucciones más sencillas. Este es uno de los motivos por los que
se segmenta la ejecución.

