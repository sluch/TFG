Una vez se ha realizado el repertorio de instrucciones y han quedado especificados
todos sus aspectos, es posible diseñar la ruta de datos. Para ello, primero se
repasarán los componentes que necesitamos y se determinarán sus características
con un nivel mayor de detalle, ya que ahora se tiene posesión de un conocimiento
que no se tenía antes de especificar el juego de instrucciones.

\subsubsection{Especificación de los componentes de la ruta de datos}
Se mostrará una visión de bloque para cada componente que toma un papel
fundamental en la ruta de datos. La implementación y la visión interna de cada
bloque se mostrará en secciones posteriores, entrando a valorar las
características que sean pertinentes.

\paragraphtitle{Unidad Aritmético Lógica (ALU)}
Aunque para realizar el diagrama de bloque de la ALU no era estrictamente
necesario haber desarrollado el juego de instrucciones, sí se han especificado
determinadas características que permitirán detallar más el diagrama, como
por ejemplo saber cuántas entradas de control serán necesarias.
La Unidad Aritmético-Lógica podrá realizar en total cinco operaciones:
\textit{ADD}, \textit{SUB}, \textit{AND}, \textit{OR} y \textit{XOR}. La
instrucción \textit{CMP} también utiliza la ALU, pero en esencia es una
operación \textit{SUB} cuyo resultado no se escribe a registro (es descartado).
Al realizar cinco acciones, necesitaremos tres bits para indicar qué acción
queremos realizar, por lo que habrá tres señales de control.

Además, es de interés que la ALU, además del resultado, genere como salida unas
banderillas o indicadores (\textit{flags}, en inglés) que aporten información
extra al programador sobre el resultado. Es común que un procesador dedique un
registro a contener todos los indicadores, normalmente llamado
\textbf{registro de estado}. Las banderillas incluidas con más frecuencia son:
\begin{itemize}
    \item \textbf{Zero Flag (ZF):}
        Esta flag se pone a 1 cuando el resultado de la ALU es cero, y se pone
        a 0 en otro caso.
    \item \textbf{Overfow Flag (OF):}
        El flag de Overflow o desbordamiento se activa en dos situaciones:
        \begin{itemize}
            \item Cuando la suma de dos positivos da como resultado un negativo.
            \item Cuando la suma de dos negativos da como resultado un positivo.
        \end{itemize}
        Por lo que responde a la fórmula:
        $(\overbar{Res}AB){\lor}(Res\overbar{A}\overbar{B})$.
        El flag de Overflow o desbordamiento es sólo relevante en operaciones
        con signo. Se encarga de avisar de que ha habido un desbordamiento en
        el resultado y que por ello el resultado que se está dando es érroneo,
        ya que hace cumplir una de las dos situaciones descritas, que son
        imposibles de satisfacer a menos que haya un error. Este es además
        el motivo por el que no tiene relevancia en operaciones sin signo.
    \item \textbf{Parity Flag (PF):}
        Esta flag indica si el resultado es par con un 0, o impar, con un 1.
    \item \textbf{Sign Flag (SF):}
        Indica si el resultado es positivo con un 0, o negativo, con un 1.
    \item \textbf{Carry Flag (CF):}
        Indica si la operación realizada necesita un bit más en la parte
        más significativa. Puede ocurrir en dos situaciones:
        \begin{itemize}
            \item La suma de dos números produce acarreo en el bit más
                significativo: $1111 + 0001 = [1]0000$.
            \item La resta de dos números produce acarreo en el bit más
                significativo: $0000 - 0001 = [1]1111$.
        \end{itemize}
\end{itemize}

\begin{figure}[H]
    \centering
    \includesvg{images/4-disenio/ALU/ALUBlock}
    \caption{Vista de bloque de la ALU.}
\end{figure}

\begin{figure}[h]
    \begin{tabular}{ |c|c|c|c| }
    \hline
        Nombre & Tipo & Ancho & Descripción \\ \hline
        \rowcolor{gris}
        A & in & 8 bits & Entrada del primer operando, \textit{Rr}. \\ \hline
        B & in & 8 bits & Entrada del segundo operando, \textit{Rs}. \\ \hline
        \rowcolor{gris}
        ALUOpX & in & 1 bit c/u & Entrada de las señales de control que permiten. \\
        \rowcolor{gris}
                          &&&seleccionar la operación a realizar. \\ \hline
        Res & out & 8 bits & Resultado de la operación. \\ \hline
        \rowcolor{gris}
        ZF & out & 1 bit & Zero Flag \\ \hline
        OF & out & 1 bit & Overflow Flag \\ \hline
        \rowcolor{gris}
        PF & out & 1 bit & Parity Flag \\ \hline
        SF & out & 1 bit & Sign Flag \\ \hline
        \rowcolor{gris}
        CF & out & 1 bit & Carry Flag \\ \hline
    \end{tabular}
    \captionof{table}{Entradas y salidas de la ALU.}
\end{figure}


\paragraphtitle{Banco de registros}
Con la información actual es posible hacer un mejor diseño del banco de registros.
Sabiendo que tendremos ocho registros de propósito general, la continuación para
hacer el diagrama de bloque es sencilla. Al igual que la ALU, el banco de registros
aceptará señales de control para determinar si se va a escribir a registro o no.

\begin{figure}[!ht]
    \centering
    \includesvg{images/4-disenio/BancoDeRegistros/BancoDeRegistrosBlock}
    \caption{Vista de bloque del banco de registros.}
    \label{fig:bancoblock}
\end{figure}


\begin{center}
    \centering
    \begin{tabular}{ |c|c|c|c| }
    \hline
        Nombre & Tipo & Ancho & Descripción \\ \hline
        \rowcolor{gris}
        ReadR1 & in & 3 bits & Selección del primer registro, \textit{Rr} \\ \hline
        ReadR2 & in & 3 bits & Selección del segundo registro, \textit{Rs} \\ \hline
        \rowcolor{gris}
        RegSel & in & 3 bits & Selección del registro resultado, \textit{Rd} \\ \hline
        WriteData & in & 8 bits & Entrada de datos para escribir en \textit{Rd} \\ \hline
        \rowcolor{gris}
        WriteEn & int & 1 bit & Entrada de la señal de control que activa la escritura \\
        \rowcolor{gris}
                        &&&en el registro seleccionado por \textit{WriteReg} de los\\
        \rowcolor{gris}
                        &&&datos recibidos por \textit{WriteData}. \\ \hline
        RData1 & out & 8 bits & Salida de los datos contenidos en el registro \\
                        &&&seleccionado por \textit{ReadR1} \\ \hline
        \rowcolor{gris}
        RData2 & out & 8 bits & Salida de los datos contenidos en el registro \\
        \rowcolor{gris}
                        &&&seleccionado por \textit{ReadR2} \\ \hline
    \end{tabular}
    \captionof{table}{Entradas y salidas del banco de registros.}
\end{center}

\paragraphtitle{Contador de Programa}
El contador de programa será un registro que se encargará de actuar como puntero
a la posición de la siguiente instrucción que ejecutar en la memoria de
instrucciones. Dado el formato de las instrucciones de salto, se podrían
direccionar directamente $2^{12}=4096$ posiciones. Tradicionalmente,
las memorias son direccionables a byte, es decir, que para guardar instrucciones
de dos bytes como las propuestas, se utilizarían dos posiciones de memoria.
En arquitecturas donde la longitud de la instrucción es fija (sobretodo en
arquitecturas RISC), se utiliza un desplazamiento a la izquierda sobre el
campo \textit{dir} de las instrucciones de slato. Aprovechando estas dos
características (direccionalidad a byte e instrucciones de longitud fija),
y sabiendo que para apuntar a una instrucción, siempre se apuntará a su primer
byte, se puede observar que con instrucciones de, por ejemplo, 16 bits, el contador
de programa siempre apuntará a instrucciones múltiplo de 2 porque apuntará al
primer byte de cada dos. Esto resulta en que el programador pierde un bit de
direccionamiento, ya que el bit menos significativo es siempre un cero explícito.
La solución consiste en hacer este cero (o ceros) implícitos: el programador
verá la memoria de instrucciones como formada por posiciones de 16 bits, en lugar
de 8. La primera instrucción estará en la posición 0, la segunda en la posición 1
(en lugar de en la 2), la tercera en la posición 2 (en lugar de en la 4), etc.,
y se desplazará el campo inmediato un bit a la izquierda para recuperar esos dos
dígitos perdidos, convirtiendo la posición 1 en la 2, la posición 2 en la 4, y así.

\begin{center}
    \texttt{0 000\textcolor{red}{0}\\
            2 001\textcolor{red}{0}\\
            4 010\textcolor{red}{0}\\
            6 011\textcolor{red}{0}\\
            8 100\textcolor{red}{0}\\}
    \textsc{Ilustración simplificado a cuatro bits}
\end{center}

Esta técnica es muy interesante y se utiliza en la mayoría de arquitecturas RISC,
pero en la nuestra no será necesario, ya que las posiciones de la memoria de
instrucciones serán directamente de 16 bits para que se presenten al programador
tal y como son en la realidad, por facilidad y visión.


\paragraphtitle{Memorias}
Ambas memorias tendrán un diagrama de bloques muy parecido:

\begin{figure}[h]
    \centering
    \includesvg{images/4-disenio/Memoria/MemoriaBlock}
    \captionof{figure}{Diagrama de la memoria.}
    \label{fig:memblock}
\end{figure}

\begin{figure}[h]
    \begin{tabular}{ |c|c|c|c| }
        \hline
        Nombre & Tipo & Ancho & Descripción \\ \hline
        \rowcolor{gris}
        din & in & \textit{m} bits & Datos que se quieran guardar en la posición \\
        \rowcolor{gris}
                &&& apuntada por \textit{dir}. \\ \hline
        dir & in & \textit{n} bits & Dirección de la que se quieren leer \\
                &&& o guardar los datos de \textit{dir} \\ \hline
        \rowcolor{gris}
        we & in & 1 bit & Señal de control que permite la escritura en \\
        \rowcolor{gris}
                &&& Memoria cuando está activada (cuando vale 1) \\ \hline
        clk & in & 1 bit & Entrada de reloj. \\ \hline
        \rowcolor{gris}
        dout & out & \textit{m} bits & Datos contenidos en la dirección indicada por \textit{dir} \\
        \hline
    \end{tabular}
    \captionof{table}{Entradas y salidas del bloque de memoria.}
\end{figure}

\vspace{1cm}


{\justifying
Siendo \textit{n} un número tal que $2^{n}$ sea igual al número de posiciones de esa
memoria. Tanto la memoria de datos como la de instrucciones tendrán 256 posiciones,
es decir, $n=8$. En la memoria de instrucciones, el ancho de cada posición será de
16 bits ($m=16$), y en la de datos será de 8 bits ($m=8$). Los datos en ambas
memorias seguirán la ordenación \textit{Big Endian}, por ser la más
intuitiva y no tener ningún otro motivo por el que .
}


\paragraphtitle{Ruta de datos}
La ruta de datos, después de incluir todos los componentes ya descritos tendría
la siguiente forma:
\begin{figure}[h]
    \includesvg[width=\textwidth]{images/4-disenio/RutaDatos/RutaDeDatos}
    \captionof{figure}{Ruta de Datos}
\end{figure}
Este diagrama incluye, en rojo, las señales de control que usaremos para controlar
el funcionamiento de la arquitectura con cada instrucción concreta. Como se puede
observar, sus componentes principales son el CP, las memorias de instrucciones y
datos, el banco de registros y la ALU. Además de eso habrá también una Unidad de
Control cuyo papel sea activar y desactivar determinadas señales de control en
función de la decodificación de la instrucción que se está ejecutando en cada
momento.


\paragraphtitle{Unidad de Control}
Aunque en la ruta de datos no aparezca la Unidad de Control, sí lo hacen las
señales que ésta genera. Para el correcto funcionamiento de la arquitectura, es
imprescindible que la UC se diseñe cuidadosamente. En el diagrama anterior
podemos ver que hay ocho señales de control que deben ser gestionadas por la
unidad de control. Además, también habrá tres señales de control en la ALU,
por lo que en total son once las señales de control que se deberán implementar.
Por claridad, las señales se han dividido en dos grupos: las tres de la ALU y las
ocho restantes.

\begin{figure}
    \centering
    \includesvg{images/4-disenio/UC/UCBlock}
    \captionof{figure}{Diagrama de Bloque de la Unidad de Control}
\end{figure}

\begin{figure}[h]
    \hspace*{-1.2cm}
    \begin{tabular}{ |c|c|c|c|c|c|c|c|c|c|c|c|c|c| }
    \hline
        & Add & Sub & Cmp & And & Or & Xor & LD & LDI & ST & STI & MOV & JMP & BEQ \\ \hline
        \rowcolor{gris}
        PCSrc      & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 & 0/1 \\ \hline
        RegWrite   & 1 & 1 & 0 & 1 & 1 & 1 & 1 & 1 & 0 & 0 & 1 & 0 & 0 \\ \hline
        \rowcolor{gris}
        WDSrc1     & 0 & 0 & - & 0 & 0 & 0 & 0 & 1 & - & - & 1 & - & - \\ \hline
        WDSrc0     & 0 & 0 & - & 0 & 0 & 0 & 1 & 1 & - & - & 0 & - & - \\ \hline
        \rowcolor{gris}
        MemWrite   & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 & 1 & 0 & 0 & 0 \\ \hline
        RR0Src     & 0 & 0 & 0 & 0 & 0 & 0 & - & 0 & 1 & 1 & 0 & 0 & 0 \\ \hline
        \rowcolor{gris}
        dinSrc     & - & - & - & - & - & - & - & - & 0 & 1 & - & - & - \\ \hline
        dirSrc     & - & - & - & - & - & - & 0 & - & 0 & 1 & - & - & - \\ \hline
        \rowcolor{gris}
        ALUOp2     & 0 & 1 & 1 & 0 & 0 & 0 & - & - & - & - & - & - & - \\ \hline
        ALUOp1     & 1 & 0 & 0 & 0 & 0 & 1 & - & - & - & - & - & - & - \\ \hline
        \rowcolor{gris}
        ALUOp0     & 1 & 1 & 1 & 0 & 1 & 0 & - & - & - & - & - & - & - \\ \hline
    \end{tabular}
    \captionof{table}{Señales de control y su valor en cada instrucción.}
    \label{tab:seniales}
\end{figure}

A continuación se hace un pequeño resumen del significado de cada señal de control.

\begin{itemize}
    \item \textbf{PCSrc}
        Controla cómo se modifica el contador de programa. Hay dos formas: se
        puede incrementar normalmente sumando 1 para que apunte a la siguiente
        instrucción de la memoria de instrucciones, o modificarlo manualmente
        con las instrucciones \textit{JMP} o \textit{BEQ}.

        \begin{center}
            \centering
            \begin{tabular} { |c|c|c| }
            \hline
                Valor & Descripción & Instrucciones \\ \hline
                \rowcolor{gris}
                0 & PC = PC + 1 & El resto \\ \hline
                1 & PC = \textit{dir} & \textit{JMP, BEQ} \\ \hline
            \end{tabular}
            \captionof{table}{Señal \textit{PCSrc}.}
        \end{center}

    \item \textbf{RegWrite}
        Controla si se escribe a los registros del banco de registros o no. Si
        esta opción está desactivada, se ignorarán los bits en la entrada de datos
        del banco de registros, \textit{WriteData}.

       \begin{center}
            \centering
            \begin{tabular} { |c|c|c| }
            \hline
                Valor & Descripción & Instrucciones \\ \hline
                \rowcolor{gris}
                0 & Ignoramos lo que hay en la & \textit{CMP, ST, STI, JMP, BEQ} \\ 
                \rowcolor{gris}
                  & entrada \textit{WriteData} & \\ \hline
                1 & Escribimos lo que hay en & \\
                  & \textit{WriteData} al registro señalado & El resto \\
                  & por \textit{WriteReg} & \\ \hline
            \end{tabular}
            \captionof{table}{Señal \textit{RegWrite}.}
        \end{center}
    
    \item \textbf{WDSrc0 y WDSrc1}
        Estas señales de control (\textit{WriteData Source}) en realidad forman lo que
        se podría considerar una única señal de control de dos bits de ancho. Su función
        es la de decidir desde dónde se va a escribir al registro apuntado por \textit{WriteReg}.
        Existirán cuatro posibles orígenes de la escritura, por eso necesitamos dos bits
        de control para el multiplexor. Los posibles orígenes son:
        \begin{itemize}
            \item \textbf{La ALU:}
                Por las instrucciones \textit{ADD}, \textit{SUB}, \textit{AND}, \textit{OR},
                \textit{XOR}.
            \item \textbf{Registro Destino (Rd):}
                Por la instrucción \textit{MOV}.
            \item \textbf{dat\_out (salida de memoria de datos):}
                Por la instrucción \textit{LD}.
            \item \textbf{Inmediato de la instrucción:}
                Por la instrucción \textit{LDI}.
        \end{itemize}

       \begin{center}
            \centering
            \begin{tabular} { |c|c|c| }
            \hline
                WDSrc1 & WDSrc0 & Instrucciones \\ \hline
                \rowcolor{gris}
                0 & 0 & \textit{ADD, SUB, AND, OR, XOR} \\ \hline
                0 & 1 & \textit{LD} \\ \hline
                \rowcolor{gris}
                1 & 0 & \textit{MOV} \\ \hline
                1 & 1 & \textit{LDI} \\ \hline
            \end{tabular}
            \captionof{table}{Señal \textit{WDSrcX}.}
        \end{center}

    \item \textbf{dinSrc:}
        Controla lo que se va a mandar a la entrada \textit{dat\_in} de la
        memoria de datos. Esta entrada se utiliza para escribir datos en dicha
        memoria.

       \begin{center}
            \centering
            \begin{tabular} { |c|c|c| }
            \hline
                Valor & Descripción & Instrucciones \\ \hline
                \rowcolor{gris}
                0 & Se carga en memoria el contenido de \textit{Rd} & \textit{ST} \\ \hline
                1 & Se carga en memoria el campo \textit{inm} & \textit{STI} \\ \hline
            \end{tabular}
            \captionof{table}{Señal \textit{dinSrc}.}
        \end{center}
    
    \item \textbf{dirSrc:}
        Controla de dónde se recibe la dirección a la que se va a escribir o de la
        que se va a leer en memoria.
        \begin{center}
            \centering
            \begin{tabular} { |c|c|c| }
            \hline
                Valor & Descripción & Instrucciones \\ \hline
                \rowcolor{gris}
                0 & La dirección se toma del campo \textit{inm} & \textit{ST, LD} \\ \hline
                1 & La dirección se toma del registro \textit{Rd} & \textit{STI} \\ \hline
            \end{tabular}
            \captionof{table}{Señal \textit{dirSrc}.}
        \end{center}

    \item \textbf{MemWrite:}
        Controla si la escritura a memoria está habilitada o deshabilitada.
       \begin{center}
            \centering
            \begin{tabular} { |c|c|c| }
            \hline
                Valor & Descripción & Instrucciones \\ \hline
                \rowcolor{gris}
                0 & No se escribe a memoria & El resto \\ \hline
                1 & Se escribe a memoria & \textit{ST, STI} \\ \hline
            \end{tabular}
            \captionof{table}{Señal \textit{MemWrite}.}
        \end{center}

    \item \textbf{RR0Src:}
        Controla de dónde se obtiene el operando que va a recibir la entrada
        \textit{ReadR1} del banco de registros.
       \begin{center}
            \centering
            \begin{tabular} { |c|c|c| }
            \hline
                Valor & Descripción & Instrucciones \\ \hline
                \rowcolor{gris}
                0 & Se recibe de \textit{Rr} & El resto \\ \hline
                1 & Se recibe de \textit{Rd} & \textit{ST, STI} \\ \hline
            \end{tabular}
            \captionof{table}{Señal \textit{RR0Src}.}
        \end{center}

    \item \textbf{ALUOp2, ALUOp1 y ALUOp0:}
        Controlan la operacion que se realizará en la ALU.
       \begin{center}
            \centering
            \begin{tabular} { |c c c|c| }
            \hline
                \multicolumn{3}{|c}{ALUOp$_{[2:0]}$} \vline & Instrucciones \\ \hline
                \rowcolor{gris}
                0 & 0 & 0 & \textit{AND} \\ \hline
                0 & 0 & 1 & \textit{OR} \\ \hline
                \rowcolor{gris}
                0 & 1 & 0 & \textit{XOR} \\ \hline
                0 & 1 & 1 & \textit{ADD} \\ \hline
                \rowcolor{gris}
                1 & 0 & 1 & \textit{SUB, CMP} \\ \hline
            \end{tabular}
            \captionof{table}{Señal \textit{ALUOpX}.}
        \end{center}
\end{itemize}
