\paragraph{Arquitectura de Computadoras:}
El concepto de Arquitectura de Computadoras ha sido históricamente difícil
de definir, muchos autores han proporcionado su visión y no siempre ha habido
convenio entre ellos. Una definición generalmente aceptada es la dada en la
presentación del IBM System/360 en 1964 por Amdahl, Blaauw y Brooks Jr~\cite{Amdahl1964}. 
Según esta definición, el término arquitectura se usa para describir los
atributos de un sistema tal y como son vistos por el programador (de 
ensamblador), es decir, la estructura conceptual y el comportamiento
funcional, a diferencia de la organización del flujo de datos y control,
el diseño lógico y la implementación física.

Sin embargo, a día de hoy una arquitectura de computadoras es más difícil de
definir debido a la evolución que han sufrido en múltiples aspectos. Es por
ello que con frecuencia se hablará de arquitectura incluyendo factores como el
diseño lógico o el hardware utilizado, que no se incluían en la definición
dada en 1964.

\paragraph{Estructura de Computadoras:}
Aunque este término provoca la misma cantidad de discusión que el anterior,
se puede decir que la estructura u organización de una computadora dan
una visión más cercana a la implementación de los componentes que conforman
la arquitectura. No sólo se ve \textit{qué} hace, sino también \textit{cómo}
lo hace. Por ejemplo, dado un componente como un sumador, se pasaría 
de una visión del componente como una caja negra que recibe unas entradas y
produce unas salidas siguiendo determinada función lógica, a una visión de
cómo el componente suma los bits hasta producir el resultado esperado. Este
ejemplo llevado a un computador completo lo que permite es pasar de una
visión útil para el programador que utilizará la arquitectura para crear
programas, a la visión de un diseñador que tendrá conocimiento de cómo se
realizan las operaciones en la computadora.

Durante el ciclo de vida de una arquitectura existirán diferentes versiones
de la organización para esa arquitectura. Por ejemplo, la arquitectura S/360
de IBM ya mencionada, sufrió numerosas extensiones a lo largo de más de 35
años~\cite{Layer2003}: S/370, S/370 Extended Architecture (XA), extensiones vectoriales
a la S/370 y S/370-XA, ESA/370 (Enterprise System Architecture), ESA/390, y
la serie S/390 G. Todas estas realizaciones eran versiones concretas de la
misma arquitectura con diferentes organizaciones, es decir, que un programador
que había aprendido y entendía la arquitectura S/360 también iba a entender la
arquitectura S/370 y todas las posteriores, sólo teniendo que estudiar las
nuevas funcionalidades incorporadas en cada una de las versiones. Otro
ejemplo podría ser la arquitectura x86, que tiene diferentes variaciones
dependiendo del objetivo de aplicación y del fabricante. La versión de Intel
de 32 bits es IA-32, mientras que la de 64 bits de AMD es x86\_64. También
tienen extensiones vectoriales como MMX, extensiones para acelerar algoritmos
criptográficos como SHA, AES-NI, etc.

\paragraph{Tecnología de fabricación:}
Cuando se habla de la tecnología de fabricación de un procesador suele ser
para hacer referencia a los materiales y los métodos con los que han sido
fabricados. La evolución de la tecnología de fabricación nos ha permitido pasar de
los tubos de vacío a los transistores bipolares, luego a los transistores
MOSFET, y al final éstos se fueron haciendo cada vez de menor tamaño, 
permitiendo concentrar un mayor número de ellos en el mismo espacio. En 1971
la tecnología comprendía transistores MOSFET de \SI{10}{\micro\metre}. A día
de hoy la tecnología de fabricación está en la escala de los
\SI{5}{\nano\metre}~\cite{web:arstechnica},
de camino hacia los \SI{3}{\nano\metre}~\cite{web:extremetech}.
Durante este proceso de evolución,
la ley de Moore ha sido especialmente relevante. Gordon Moore observó en 1965
que el número de componentes en los circuitos integrados se doblaba cada
año~\cite{MooreLaw}
(corregido en 1975 a cada dos años~\cite{Tuomi2002}). Este efecto ha sido observable en
la reducción del tamaño de los componentes electrónicos que se usan cada
día. Esta ley se mantuvo indisputada hasta recientemente, que el ritmo de 
reducción de tamaño ha ido frenando poco a poco.


\paragraph{Arquitectura Von Neumann o Harvard:}
Una clasificación común que se da a las arquitecturas es según la organización
de su memoria principal. Una arquitectura Von Neumann tendrá una sola memoria donde
estarán tanto las instrucciones a ejecutar como los datos con los que las
instrucciones operarán. En una arquitectura Harvard habrá una memoria para las
instrucciones y otra para los datos. La mayoría de soluciones a día de hoy son
soluciones híbridas principalmente en la jerarquía de cachés.


\paragraph{Lógica combinacional y secuencial:}
Se denomina lógica combinacional a todo circuito lógico cuya salida depende únicamente
del valor de sus entradas presentes, a diferencia de la lógica secuencial, cuyo valor
de salida depende tanto del valor de sus entradas como de su valor pasado. La lógica
combinacional no tiene memoria, y la secuencial sí la tiene.

En la práctica, esta diferencia se ve en diferentes circuitos. Los decodificadores,
sumadores, multiplexores, o enablers son circuitos combinacionales, mientras que los
registros y las memorias son circuitos secuenciales, porque el valor de su salida
depende también de los estados anteriores del circuito.


\paragraph{Instrucción:}
Una instrucción es la orden mínima que una computadora será capaz de ejecutar.
Las instrucciones suelen clasificarse en tres categorías:
\begin{itemize}
    \item \textbf{Instrucciones aritmético-lógicas:}
        En este grupo se encuentran instrucciones de suma, resta, multiplicación,
        división, además de instrucciones lógicas como AND, OR o NOT lógico. Estas
        operaciones se podrán aplicar a uno, dos, o más datos, dependiendo del
        repertorio de instrucciones. Ejemplos de estas
        instrucciones son \textit{ADD}, \textit{MUL}, \textit{XOR}, \textit{AND}.
    \item \textbf{Instrucciones de transferencia:}
        En este grupo se encuentran instrucciones que permitirán mover datos
        de un lugar a otro de la computadora. Por ejemplo, mover un dato de un
        registro a otro, de un registro a memoria, o de memoria a un registro.
        Ejemplos de estas instrucciones son \textit{MOV}, \textit{LD}, \textit{ST}.
    \item \textbf{Instrucciones de salto o control de flujo:}
        Estas instrucciones permitirán controlar el flujo con el que se ejecutan
        las instrucciones en una computadora. Normalmente se ejecutan de manera
        secuencial, una después de otra, pero con estas operaciones podremos saltar
        a otros puntos de la cadena de ejecución. Ejemplos de estas instrucciones
        son \textit{JMP}, \textit{BEQ}, \textit{BNEQ}.
\end{itemize}
Al conjunto de todas las instrucciones que una arquitectura es capaz de ejecutar
lo llamaremos repertorio, juego de instrucciones, o ISA, de sus siglas en
inglés: \textit{Instruction Set Architecture}. Además, a cada instrucción del
repertorio se la identificará por un código de operación u \textit{opcode}. A
partir de este opcode, la computadora podrá actuar de formas diferentes dependiendo
de las necesidades de cada instrucción, a saber: activar escritura a registros,
activar escritura a memoria, habilitar la modificación del registro contador de
programa, decidir desde qué entrada se escribe a los registros, etc.


\paragraph{RISC vs CISC:}
Se denomina CISC (\textit{Complex Instruction Set Architecture}) a aquellos repertorios
con un gran número de instrucciones complejas, muchos tipos de datos, modos de direccionamiento
u operaciones. Los repertorios RISC siguen la filosofía contraria. De sus siglas en inglés,
\textit{Reduced Instruction Set Architecture}, están compuesto por un número reducido de
instrucciones básicas y simples, pocos modos de direccionamiento y pocos tipos de datos.

Las ventajas de los repertorios CISC es que pueden implementar funciones complejas con pocas
instrucciones en ensamblador. Esto es beneficioso en algunos escenarios en los que el
espacio que ocupan las instrucciones es limitado, como en el contexto de las memorias caché,
que cualquier disminución en el tamaño del código es ventajoso para el sistema.

Los repertorios RISC ganaron popularidad desde los años 80 gracias a múltiples factores. A
partir de un repertorio RISC es sencillo diseñar otro repertorio nuevo con algunas modificaciones
o extensiones, lo cual es útil para añadir funcionalidad como cálculo vectorial a uno
que no incorporaba inicialmente instrucciones de esa naturaleza. Además, el hardware de este tipo
de procesadores es más sencillo de diseñar y aceptan una mayor frecuencia de reloj. Otro motivo es
que las técnicas de optimización son más sencillas de implementar, tanto en el propio procesador
como en el compilador que resultará para la arquitectura creada.

Un ejemplo sencillo para visualizar la diferencia entre una arquitectura RISC y una CISC: 
un caso en el que se tenga que obtener dos valores de memoria, multiplicarlos, y guardar el
resultado de nuevo en memoria. En una arquitectura CISC, esto podría hacerse en una sola
instrucción \textit{MULT} que moviese los dos valores de memoria a registros, realizase la
multiplicación, y guardase el resultado en memoria. Sin embargo, en una aproximación RISC,
se utilizarían dos instrucciones \textit{LOAD} para cargar los valores de memoria en los dos
registros, luego una instrucción \textit{MUL} para multiplicar esos dos datos, y finalmente
una instrucción \textit{STORE} para guardar el producto en memoria.

Es habitual que las arquitecturas RISC sigan un modelo de carga-almacenamiento (load-store
en inglés). En estas arquitecturas, las instrucciones pueden dividirse en dos grupos: el
grupo de las instrucciones de carga-almacenamiento (aquellas que acceden a memoria para
leer o escribir) y el grupo de las instrucciones que acceden a la ALU para realizar
operaciones aritmético-lógicas. En las arquitecturas CISC, esta aproximación no se da,
ya que conviene que las instrucciones realicen varias tareas.


\paragraph{Palabra:}
Una palabra es la unidad mínima de información con la que podrá operar la 
arquitectura, es decir, la unidad mínima de información direccionable.
A día de hoy, por motivos históricos, el significado estricto de lo que significa
la palabra de una arquitectura está diluído. Sin embargo, es común aceptar que
el tamaño de los registros definen la palabra de una arquitectura. Además, es
común que la memoria principal de una arquitectura sea siempre direccionable a 
byte, es decir, que su palabra sea de 8 bits aunque la palabra de la arquitectura
sea de 32 o de 64. Lo que significa esto es que a la hora de mover datos de
la memoria, por ejemplo, sólo se podrá hacer con datos cuyo tamaño sea múltiplo de
la palabra. Así, en una memoria con palabra de 8 bits, se podrían mover los datos de
8 bits en 8 bits, o de 16 en 16, pero no de 7 en 7 ni de 21 en 21.


\paragraph{Ordenación de la palabra:}

La ordenación de la palabra, también llamada \textbf{endianness} en inglés, es
el orden que se utiliza para representar sus bits. Hay dos opciones:
\begin{itemize}
    \item \textbf{Big Endian:}
        Se coloca el byte menos significativo en la posición menos significativa
        de la palabra de memoria. Por ejemplo, para representar en memoria el valor 
        0x0A1B2C3D en Big Endian, se representaría por la secuencia {0A, 1B, 2C, 3D}.
    \item \textbf{Little Endian:}
        Se coloca el byte menos significativo en la posición más significativa.
        Con el mismo ejemplo anterior, la representación en memoria sería la
        secuencia {3D, 2C, 1B, 0A}.
\end{itemize}

A algunas arquitecturas que son capaces de utilizar ambas ordenaciones se las suele
denominar \textbf{Middle Endian}.


\paragraph{Representación de la información:}
Cualquier computadora trata toda la información como cadenas de bits. A nivel
humano, sin embargo, se dispone de diferentes tipos de datos: sistemas numerales,
sistemas alfabéticos, diferentes símbolos tipográficos, etc. Para representar
cada tipo de datos en la computadora se tendrá que llegar a convenio sobre la
codificación de cada dato. Luego, a más alto nivel, se interpretarán las
cadenas de bits como pertenecientes a una codificación o a otra.

Un ejemplo de esta codificación es el estándar ASCII para representar caracteres
alfanuméricos. Según ésta codificación, la letra \textit{A}, sería en binario
\textit{0100 0001}.

Para la representación de numerales enteros, existen tres alternativas posibles:
\begin{itemize}
    \item \textbf{Signo-magnitud:}
        Según esta representación, se utiliza un bit extra que servirá como
        signo. El valor 0 indicará que el número es positivo y el valor 1, que
        es negativo.\\
        Por ejemplo, usando cuatro bits y uno de signo:
        \emph{12} sería \emph{01100}; y \emph{-12} sería \emph{11100}.\\
        Este sistema es sencillo, pero trae consigo problemas de operabilidad:
        el cero tendría dos representaciones: una positiva y una
        negativa, y esto implicaría tener que hacer ajustes en las sumas y las
        restar si el resultado cambia de signo. 
    \item \textbf{Complemento a uno:}
        Como solución parcial surge el complemento a uno, que consiste en invertir
        todos los bits de un número para cambiar su signo. Así, \emph{12} seguiría
        siendo \emph{01100}, pero \emph{-12} sería \emph{10011}. En esta
        representación es más fácil de operar que con signo-magnitud, pero se
        conserva la doble representación del cero.
    \item \textbf{Complemento a dos:}
        El complemento a dos se define matemáticamente como
        \[C_{2}^{N}=2^{n}-N\] donde \emph{n} es el número máximo de bits (5 si
        continuamos con ejemplo) y \emph{N} es el número que se desea obtener en complemento
        a dos.\\
        Esta representación es la más usada en arquitecturas de computadoras modernas,
        ya que elimina la doble representación del cero y facilita mucho la operación
        aritmética entre números positivos y negativos.
\end{itemize}
