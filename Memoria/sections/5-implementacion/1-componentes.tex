Durante toda la etapa de implementación en VHDL se mantuvo el objetivo de que
tanto cada componente como el funcionamiento conjunto de todos ellos fuera
sintetizable, es decir, directamente transferible al hardware de una FPGA.
Para que esto fuera posible los componentes más básicos que iban a ser usados
en otros más complejos se realizaron primero. De esta forma, para hacer un
sumador de 8 bits se utilizarán dos sumadores de 4 bits y éstos a su vez
harían uso de cuatro sumadores totales. En esta sección se mostrará cómo se
ha implementado cada componente partiendo únicamente de puertas lógicas y otros
componentes generados manualmente.

\subsubsection{Sumador total}
El sumador total es la unidad funcional mínima de suma con acarreo. 
Será el componente utilizado en el desarrollo del cuádruple sumador total,
detallado en el siguiente párrafo.
\paragraphtitle{Descripción de las entradas}
\begin{center}
    \centering
    \begin{tabular} { |c|c|c|c| }
    \hline
        Nombre & Tipo & Ancho & Descripción \\ \hline
        \rowcolor{gris}
        a & in & 1 bit & Primer operando. \\ \hline
        b & in & 1 bit & Segundo operando. \\ \hline
        \rowcolor{gris}
        Cin & in & 1 bit & Acarreo de entrada. \\ \hline
        S & out & 1 bit & Resultado de la suma. \\ \hline
        \rowcolor{gris}
        Cout & out & 1 bit & Acarreo de salida. \\
    \hline
    \end{tabular}
    \captionof{table}{Entradas y salidas del sumador total.}
\end{center}

\paragraphtitle{Tabla de verdad y bloque}
\begin{figure}[H]
    \hspace*{-4cm}
    \capbtabbox{%
        \begin{tabular} { |c|c|c|c|c|c| }
        \hline
            Cin & a & b & Cout & S \\ \hline
            \rowcolor{gris}
            0 & 0 & 0 & 0 & 0 \\ \hline
            0 & 0 & 1 & 0 & 1 \\ \hline
            \rowcolor{gris}
            0 & 1 & 0 & 0 & 1 \\ \hline
            0 & 1 & 1 & 1 & 0 \\ \hline
            \rowcolor{gris}
            1 & 0 & 0 & 0 & 1 \\ \hline
            1 & 0 & 1 & 1 & 0 \\ \hline
            \rowcolor{gris}
            1 & 1 & 0 & 1 & 0 \\ \hline
            1 & 1 & 1 & 1 & 1 \\ 
        \hline
        \end{tabular}
    }{%
        \captionof{table}{Tabla de verdad del sumador total.}
    }
    \hspace*{-8cm}
    \ffigbox{%
        \includesvg{images/5-implementacion/0-comp/Adders/FullAdder/FullAdderBlock}
    }{%
        \captionof{figure}{Vista de bloque del sumador total.}
    }
\end{figure}

\paragraphtitle{Función Lógica}
\begin{figure}[h]
    \centering
    $S \gets a \oplus b \oplus c$\\
    $Cout \gets (a \land b) \lor (a \land c) \lor (b \land c)$
    \captionof{figure}{Función lógica de las salidas del sumador total.}
\end{figure}

\paragraphtitle{Simulación}
\begin{center}
    \centering
    \includegraphics{images/5-implementacion/0-comp/Adders/FullAdder/FullAdderSim.png}
    \captionof{figure}{Simulación del sumador total.}
\end{center}



\subsubsection{Cuádruple sumador total}
El cuádruple sumador total sumará dos operandos de cuatro bits y un bit de
acarreo. Produce un resultado de cuatro bits más un acarreo de salida.

\paragraphtitle{Descripción de las entradas}
\begin{center}
    \centering
    \begin{tabular} { |c|c|c|c| }
    \hline
        Nombre & Tipo & Ancho & Descripción \\ \hline
        \rowcolor{gris}
        a & in & 4 bits & Primer operando. \\ \hline
        b & in & 4 bits & Segundo operando. \\ \hline
        \rowcolor{gris}
        Cin & in & 1 bit & Acarreo de entrada. \\ \hline
        S & ou & 4 bits & Resultado de la suma. \\ \hline
        \rowcolor{gris}
        Cout & ou & 4 bits & Acarreo de salida. \\ 
    \hline
    \end{tabular}
    \captionof{table}{Entradas del cuádruple sumador total.}
\end{center}

\paragraphtitle{Bloque}
\begin{center}
    \centering
    \includesvg{images/5-implementacion/0-comp/Adders/Add4bits/Add4bitsBlock}
    \captionof{figure}{Vista de bloque del cuádruple sumador total.}
\end{center}

\paragraphtitle{Esquemático}
El cuádruple sumador total está formado a partir de cuatro sumadores totales
conectados entre sí.
\begin{center}
    \centering
    \includesvg[scale=0.7]{images/5-implementacion/0-comp/Adders/Add4bits/Add4bits}
    \captionof{figure}{Vista interna del cuádruple sumador total.}
\end{center}

\paragraphtitle{Simulación}
\begin{center}
    \centering
    \includegraphics[scale=0.6]{images/5-implementacion/0-comp/Adders/Add4bits/Add4bitsSim.png}
    \captionof{figure}{Simulación del cuádruple sumador total.}
\end{center}


\subsubsection{Sumador de 8 bits}
El sumador de ocho bits funciona de manera análoga al sumador de cuatro bits, pero
con entradas y salidas \textit{a, b} y \textit{S} de anchura 8 bits.

\paragraphtitle{Descripción de las entradas}
\begin{center}
    \centering
    \begin{tabular} { |c|c|c|c| }
    \hline
        Nombre & Tipo & Ancho & Descripción \\ \hline
        \rowcolor{gris}
        a & in & 8 bits & Primer operando. \\ \hline
        b & in & 8 bits & Segundo operando. \\ \hline
        \rowcolor{gris}
        Cin & in & 1 bit & Acarreo de entrada. \\ \hline
        S & ou & 8 bits & Resultado de la suma. \\ \hline
        \rowcolor{gris}
        Cout & ou & 8 bits & Acarreo de salida. \\ 
    \hline
    \end{tabular}
    \captionof{table}{Entradas y salidas del sumador de 8 bits.}
\end{center}

\paragraphtitle{Bloque}
\begin{center}
    \centering
    \includesvg{images/5-implementacion/0-comp/Adders/Add8bits/Add8bitsBlock}
    \captionof{figure}{Vista de bloque del sumador de 8 bits.}
\end{center}

\paragraphtitle{Esquemático}
El sumador de ocho bits está formado a partir de dos cuádruples sumadores totales
conectados entre sí.
\begin{center}
    \centering
    \includesvg{images/5-implementacion/0-comp/Adders/Add8bits/Add8bits}
    \captionof{figure}{Vista interna del sumador de 8 bits.}
\end{center}

\paragraphtitle{Simulación}
\begin{center}
    \centering
    \includegraphics[scale=0.5]{images/5-implementacion/0-comp/Adders/Add8bits/Add8bitsSim.png}
    \captionof{figure}{Simulación del sumador de 8 bits.}
\end{center}


\subsubsection{Decodificador de 2 a 4}
Los decodificadores convierten un código binario de $n$ bits de entradas a $m$ salidas.
Cada entrada activa únicamente una salida. En los decodificadores usados en este
proyecto se cumple que $m = 2^{n}$.


\paragraphtitle{Descripción de las entradas}
\begin{center}
    \begin{tabular} { |c|c|c|c| }
    \hline
        Nombre & Tipo & Ancho & Descripción \\ \hline
        \rowcolor{gris}
        en & in & 1 & Entrada de activación. \\ \hline
        e & in & 2 & Entrada de selección. \\ \hline
        \rowcolor{gris}
        S & in & 4 & Salida decodificada. \\
    \hline
    \end{tabular}
    \captionof{table}{Entradas y salidas del decodificador de 2 a 4.}
\end{center}

\paragraphtitle{Tabla de verdad y bloque}
\begin{figure}[H]
    \hspace*{-4cm}
    \capbtabbox{%
        \begin{tabular} { |c|c c|c c c c| }
            \hline
            en & \multicolumn{2}{c}{e$_{[1:0]}$} \vline & \multicolumn{4}{c}{S$_{[3:0]}$} \vline \\ \hline
            \rowcolor{gris}
            0 & - & - & 0 & 0 & 0 & 0 \\ \hline
            1 & 0 & 0 & 0 & 0 & 0 & 1 \\ \hline
            \rowcolor{gris}
            1 & 0 & 1 & 0 & 0 & 1 & 0 \\ \hline
            1 & 1 & 0 & 0 & 1 & 0 & 0 \\ \hline
            \rowcolor{gris}
            1 & 1 & 1 & 1 & 0 & 0 & 0 \\ 
            \hline
        \end{tabular}
    }{%
        \captionof{table}{Tabla de verdad del decodificador de 2 a 4.}
    }
    \hspace*{-8cm}
    \ffigbox{%
        \includesvg{images/5-implementacion/0-comp/Decod/Decod2_4/Decod2_4Block}
    }{%
        \captionof{figure}{Vista de bloque del\\decodificador de 2 a 4.}
    }
\end{figure}

\paragraphtitle{Función lógica}
\begin{center}
    \centering
    $S_{0} = (en)\land(\overbar{e_{1}}\land \overbar{e_{0}})$ \\
    $S_{1} = (en)\land(\overbar{e_{1}}\land {e_{0}})$ \\
    $S_{2} = (en)\land(e_{1}\land \overbar{e_{0}})$ \\
    $S_{3} = (en)\land(e_{1}\land e_{0})$ \\
    \captionof{figure}{Función lógica de las salidas del decodificador de 2 a 4.}
\end{center}

\paragraphtitle{Simulación}
\begin{center}
    \centering
    \includegraphics{images/5-implementacion/0-comp/Decod/Decod2_4/Decod2_4Sim.png}
    \captionof{figure}{Simulación del decodificador de 2 a 4.}
\end{center}


\subsubsection{Decodificador de 3 a 8}
Su funcionamiento es análogo al del decodificador de 2 a 4. Para su implementación
se han usado dos de éstos decodificadores.

\paragraphtitle{Descripción de las entradas}
\begin{center}
    \vspace*{-0.5cm}
    \centering
    \begin{tabular} { |c|c|c|c| }
    \hline
        Nombre & Tipo & Ancho & Descripción \\ \hline
        \rowcolor{gris}
        en & in & 1 bit & Entrada de activación. \\ \hline
        e & in & 3 bits & Entrada de selección. \\ \hline
        \rowcolor{gris}
        S & in & 8 bits & Salida decodificada. \\
    \hline
    \end{tabular}
    \captionof{table}{Entradas y salids del decodificador de 3 a 8.}
\end{center}


\paragraphtitle{Bloque y esquemático}
\begin{figure}[H]
\RawFloats
    \centering
    \begin{minipage}[b]{0.4\textwidth}
        \includesvg{images/5-implementacion/0-comp/Decod/Decod3_8/Decod3_8Block}
        \captionof{figure}{Vista de bloque del decodificador de 3 a 8.}
    \end{minipage}
    \hfill
    \begin{minipage}[b]{0.4\textwidth}
        \includesvg{images/5-implementacion/0-comp/Decod/Decod3_8/Decod3_8}
        \captionof{figure}{Vista interna del decodificador de 3 a 8.}
    \end{minipage}
\end{figure}

\paragraphtitle{Simulación}
\begin{center}
    \centering
    \includegraphics[scale=0.7]{images/5-implementacion/0-comp/Decod/Decod3_8/Decod3_8Sim.png}
    \captionof{figure}{Simulación del decodificador de 3 a 8.}
\end{center}


\subsubsection{Decodificador de 4 a 16}
Funciona de manera análoga a los otros decodificadores. La entrada \textit{e}
es de cuatro bits de ancho, y la salida \textit{S} de 16.
Para su implementación se han usado cinco decodificadores de 2 a 4. Al ser
análogo a los otros decodificadores, sólo se mostrarán su bloque y el
diseño de su esquemático.

\paragraphtitle{Bloque}
\begin{center}
    \centering
    \includesvg{images/5-implementacion/0-comp/Decod/Decod4_16/Decod4_16Block}
    \captionof{figure}{Vista de bloque del decodificador de 4 a 16.}
\end{center}

\paragraphtitle{Esquemático}
\begin{center}
    \centering
    \includesvg{images/5-implementacion/0-comp/Decod/Decod4_16/Decod4_16}
    \captionof{figure}{Vista interna del decodificador de 4 a 16.}
\end{center}

\paragraphtitle{Simulación}
\begin{center}
    \centering
    \includegraphics[width=\textwidth]{images/5-implementacion/0-comp/Decod/Decod4_16/Decod4_16Sim.png}
\end{center}



\subsubsection{Enabler Genérico y Enabler Genérico Xor}
Estos componentes se utilizan para habilitar o deshabilitar un bus de \textit{n}
bits de ancho. El Enabler normal habilita todos los bits de entrada tal y como
están cuando \textit{Z} está a nivel alto. El Enabler Xor realiza la función \textit{XOR}
con la entrada \textit{Z} sobre cada bit del bus.
\paragraphtitle{Descripción de las entradas}
\begin{center}
    \centering
    \begin{tabular} { |c|c|c|c| }
    \hline
        Nombre & Tipo & Ancho & Descripción \\ \hline
        \rowcolor{gris}
        a & in & \textit{n} & Bus de entrada. \\ \hline
        Z & in & 1 & Entrada de activación. \\ \hline
        \rowcolor{gris}
        S & out & \textit{n} & Bus de salida. \\
    \hline
    \end{tabular}
    \captionof{table}{Entradas y salidas de los enablers.}
\end{center}

\paragraphtitle{Bloques}
Ambos componentes tienen el mismo bloque.
\begin{center}
    \centering
    \includesvg{images/5-implementacion/0-comp/Enablers/GenericEnabler/GenericEnablerBlock}
    \captionof{figure}{Vista de bloque de los enablers.}
\end{center}

\paragraphtitle{Función Lógica}
\begin{center}
    \begin{itemize}
        \item \textbf{Enabler:} $S_{i} = a_{i} \land Z$
        \item \textbf{Enabler Xor:} $S_{i} = a_{i} \oplus Z$
    \end{itemize}
    \captionof{table}{Funciones lógicas de las salidas de los enablers.}
\end{center}

\paragraphtitle{Simulación}
\begin{center}
    \includegraphics[scale=0.6]{images/5-implementacion/0-comp/Enablers/GenericEnabler/GenericEnablerSim.png}
    \captionof{figure}{Simulación del enabler.}
\end{center}
\begin{center}
    \includegraphics[scale=0.6]{images/5-implementacion/0-comp/Enablers/GenericXorEnabler/GenericXOREnablerSim.png}
    \captionof{figure}{Simulación del enabler Xor.}
\end{center}


\newpage
\subsubsection{Multiplexor Genérico de 2 a 1}
Un multiplexor de dos entradas y un bit de selección. Las entradas y las salidas
tienen una anchura de bus de \textit{n} bits.
\paragraphtitle{Descripción de las entradas y tabla de verdad}
\begin{figure}[H]
    \centering
    \subfloat{%
        \begin{tabular} { |c|c|c|c| }
        \hline
            Nombre & Tipo & Ancho & Descripción \\ \hline
            \rowcolor{gris}
            a & in & \textit{n} & Primera entrada. \\ \hline
            b & in & \textit{n} & Segunda entrada. \\ \hline
            \rowcolor{gris}
            Z & in & 1 & Entrada de selección. \\ \hline
            S & out & \textit{n} & Bus de salida. \\
        \hline
        \end{tabular}
    }
    \hspace{1cm}
    \subfloat{%
        \begin{tabular} { |c|c| }
        \hline
            Z & S \\ \hline
            \rowcolor{gris}
            0 & a \\ \hline
            1 & b \\
        \hline
        \end{tabular}
    }
    \captionof{table}{Entradas, salidas y tabla de verdad del Multiplexor 2 a 1.}
\end{figure}

\paragraphtitle{Bloque y esquemático}
En el componente utilizamos un \textit{Enabler} de \textit{n} bits de ancho, y un
OR del mismo ancho.
\begin{figure}[H]
\RawFloats
    \centering
    \begin{minipage}[b]{0.4\textwidth}
        \includesvg{images/5-implementacion/0-comp/Mux/GenerixMux2_1/GenericMux2_1Block}
        \captionof{figure}{Vista de bloque del multiplexor de 2 a 1.}
    \end{minipage}
    \hfill
    \begin{minipage}[b]{0.4\textwidth}
        \includesvg{images/5-implementacion/0-comp/Mux/GenerixMux2_1/GenericMux2_1}
        \captionof{figure}{Esquemático del multiplexor de 2 a 1.}
    \end{minipage}
\end{figure}

\newpage
\paragraphtitle{Simulación}
\vspace*{-30 pt}
\begin{figure}[h]
    \centering
    \includegraphics{images/5-implementacion/0-comp/Mux/GenerixMux2_1/GenericMux2_1Sim.png}
    \captionof{figure}{Simulación del multiplexor de 2 a 1.}
\end{figure}

\vspace*{-30pt}
\subsubsection{Multiplexor Genérico de 4 a 2}
Un multiplexor de cuatro entradas y dos bits de selección. Al ser análogo al
multiplexor anterior, se expondrán brevemente su tabla de verdad, su
esquemático y su simulación.

\paragraphtitle{Esquemático y tabla de verdad}
\begin{figure}[H]
    \begin{floatrow}
        \ffigbox{%
            \includesvg{images/5-implementacion/0-comp/Mux/GenerixMux4_2/GenericMux4_2}
        }{%
            \captionof{figure}{Esquemático del multiplexor de 4 a 2.}%
        }
        \capbtabbox{%
            \begin{tabular} { |c c|c| } 
                \hline
                \multicolumn{2}{|c}{Z$_{[1:0]}$} \vline & S \\ \hline
                \rowcolor{gris}
                0 & 0 & a \\ \hline
                0 & 1 & b \\ \hline
                \rowcolor{gris}
                1 & 0 & c \\ \hline
                1 & 1 & d \\
                \hline
            \end{tabular}
        }{%
            \captionof{figure}{Tabla de verdad del multiplexor de 4 a 2.}%
        }
    \end{floatrow}
\end{figure}

\paragraphtitle{Simulación}
\vspace*{-20 pt}
\begin{figure}[H]
    \hspace*{-1cm}
    \includegraphics{images/5-implementacion/0-comp/Mux/GenerixMux4_2/GenericMux4_2Sim.png}
    \captionof{figure}{Simulación del multiplexor 4 a 2.}
\end{figure}

\subsubsection{Multiplexor genérico de 8 a 3}
Ocho entradas y tres bits de selección. Por el mismo motivo, se expondrá brevemente su
tabla de verdad, su esquemático y su simulación.

\newpage
\paragraphtitle{Esquemático y tabla de verdad}
\vspace*{-2cm}
\begin{figure}[H]
    \begin{floatrow}
        \ffigbox{%
            \hspace*{-2cm}
            \includesvg{images/5-implementacion/0-comp/Mux/GenerixMux8_3/GenericMux8_3}
        }{%
            \captionof{figure}{Esquemático del\\multiplexor de 8 a 3.}%
        }
        \capbtabbox{%
            \hspace*{1cm}
            \begin{tabular} { |c c c|c| } 
                \hline
                \multicolumn{3}{|c}{Z$_{[2:0]}$} \vline & S \\ \hline
                \rowcolor{gris}
                0 & 0 & 0 & a \\ \hline
                0 & 0 & 1 & b \\ \hline
                \rowcolor{gris}
                0 & 1 & 0 & c \\ \hline
                0 & 1 & 1 & d \\ \hline
                \rowcolor{gris}
                1 & 0 & 0 & e \\ \hline
                1 & 0 & 1 & f \\ \hline
                \rowcolor{gris}
                1 & 1 & 0 & g \\ \hline
                1 & 1 & 1 & h \\
                \hline
            \end{tabular}
        }{%
        \captionof{table}{Tabla de verdad del multiplexor de 8 a 3.}%
        }
    \end{floatrow}
\end{figure}

\paragraphtitle{Simulación}
\begin{figure}[H]
    \includegraphics[scale=0.8]{images/5-implementacion/0-comp/Mux/GenerixMux8_3/GenericMux8_3Sim.png}
    \captionof{figure}{Simulación del multiplexor 8 a 3.}
\end{figure}


\subsubsection{Biestable SR con entrada de activación}
Para poder realizar más adelante componentes secuenciales como los registros o
las memorias siguiendo el mismo patrón de diseño que con todos los componentes
anteriores, era necesario realizar la unidad mínima de almacenamiento y
utilizarla como bloque de construcción. Este es el papel de los biestables: 
pueden almacenar un bit de información. Particularmente, este tipo de biestable
cuenta con una entrada de activación que mantendrá el estado del biestable cuando
esté inactiva.

\paragraphtitle{Descripción de las entradas y tabla de verdad}
\begin{table}[H]
    \subfloat{%
        \begin{tabular} { |c|c|c|c| }
        \hline
            Nombre & Tipo & Ancho & Descripción \\ \hline
            \rowcolor{gris}
            E & in & 1 & Entrada de activación. \\ \hline
            S & in & 1 & Entrada de Set. \\ \hline
            \rowcolor{gris}
            R & in & 1 & Entrada de Reset. \\ \hline
            Q & out & 1 & Salida. \\ \hline
            \rowcolor{gris}
            $\overbar{Q}$ & out & 1 & Salida negada. \\
        \hline
        \end{tabular}
    }
    \hspace{0.5cm}
    \subfloat{%
        \begin{tabular} { |c|c|c|c| }
        \hline
            En & R & S & Q$_{t+1}$ \\ \hline
            \rowcolor{gris}
            0 & - & - & Q$_{t}$ \\ \hline
            1 & 0 & 0 & Q$_{t}$ \\ \hline
            \rowcolor{gris}
            1 & 0 & 1 & 1 \\ \hline
            1 & 1 & 0 & 0 \\ \hline
            \rowcolor{gris}
            1 & 1 & 1 & \textit{Prohibido} \\
        \hline
        \end{tabular}
    }
    \captionof{table}{Entradas, salidas, y tabla de verdad del biestable SR.}
\end{table}

\paragraphtitle{Bloque y esquemático}
Suele recomendarse escribir el comportamiento de los biestables en VHDL de forma
conductual (\textit{behavioural}). Por esto, el esquemático generado a partir de VHDL en
Vivado no es representativo de cómo es la estructura interna de estos biestables en
la realidad. En esta sección se proporciona uno de los posibles esquemáticos comunmente
utilizados en este tipo de biestable.
\begin{figure}[H]
\RawFloats
    \centering
    \begin{minipage}[b]{0.4\textwidth}
        \includesvg{images/5-implementacion/0-comp/Latches/SR_Latch_En/SR_Latch_EnBlock}
        \captionof{figure}{Vista de bloque del biestable SR.}
    \end{minipage}
    \hfill
    \begin{minipage}[b]{0.4\textwidth}
        \includesvg{images/5-implementacion/0-comp/Latches/SR_Latch_En/SR_Latch_En}
        \captionof{figure}{Vista interna de un biestable SR NAND.}
    \end{minipage}
\end{figure}

\paragraphtitle{Simulación}
\begin{figure}[H]
    \includegraphics{images/5-implementacion/0-comp/Latches/SR_Latch_En/SR_Latch_EnSim.png}
    \captionof{figure}{Simulación del biestable SR.}
\end{figure}


\subsubsection{Registro genérico}
Este diseño permitirá reciclar su uso cuando sea necesario, de forma que podremos
crear registros de 8 bits, de 16, o de cualquier cantidad de bits. Utilizan los
biestables SR.

\paragraphtitle{Descripción de las entradas y bloque}
El funcionamiento del registro es muy sencillo: Si la entrada \textit{en} está activa,
se escribirá en el registro el dato introducido por \textit{dataIn}. Si \textit{en}
está inactiva, se mantendrá el contenido de los registros. \textit{dataOut} siempre
devuelve el contenido del registro instantáneamente (en el mismo ciclo), incluso tras un cambio.
\begin{figure}[H]
    \begin{floatrow}
        \capbtabbox{%
        \begin{tabular} { |c|c|c|c| }
        \hline
            Nombre & Tipo & Ancho & Descripción \\ \hline
            \rowcolor{gris}
            en & in & 1 & Entrada de activación. \\ \hline
            dataIn & in & \textit{n} & Entrada de datos. \\ \hline
            \rowcolor{gris}
            dataOut & out & \textit{n} & Salida de datos. \\
        \hline
        \end{tabular}
        }{%
            \captionof{table}{Tabla de verdad del registro genérico.}
        }
        \ffigbox{%
            \includesvg{images/5-implementacion/0-comp/Registers/GenericReg/GenericRegBlock}
        }{%
            \captionof{figure}{Vista de bloque del registro genérico.}
        }
    \end{floatrow}
\end{figure}

\newpage
\paragraphtitle{Esquemático y simulación}
\begin{figure}[H]
\RawFloats
    \centering
    \begin{minipage}[b]{0.4\textwidth}
        \includegraphics[scale=0.5]{images/5-implementacion/0-comp/Registers/GenericReg/GenericRegSim.png}
        \captionof{figure}{Simulación del registro genérico.}
    \end{minipage}
    \hfill
    \begin{minipage}[b]{0.4\textwidth}
        \includesvg{images/5-implementacion/0-comp/Registers/GenericReg/GenericReg}
        \captionof{figure}{Esquemático del registro con $n=4$.}
    \end{minipage}
\end{figure}
