
# 26 de Enero de 2021
 Empiezo a realizar el TFG. Mi tutor me recomienda empezar haciendo el juego de
 instrucciones directamente. El problema con esto es que para empezar a hacerlo
 necesito tomar determinadas decisiones que todavía no me siento capacitado para
 tomar.
 Ejemplo de decisiones: Tipo de almacenamiento de los operandos, modos de 
 direccionamiento, longitud variable o fija de las instrucciones. 
 Todo esto sumado a que he encontrado que hay una característica de los juegos
 de instrucciones llamada ortogonalidad, que es muy deseable para crear los
 compiladores de esa arquitectura.

 Además, he estado buscando otras arquitecturas para ver cómo han hecho las
 decisiones de diseño, pero sin mucho éxito. Las que he encontrado por ahora 
 son las siguientes:
   + Arquitectura AVR (usada en los procesadores de Arduino)
   + Arquitectura nanoMIPS 
   + Arquitectura RISC-V u Open-RISC

 Como para empezar con el set de instrucciones tengo que tomar estas decisiones,
 lo que estoy haciendo es leer la bibliografía marcada para asentar las bases
 teóricas y reforzar conceptos que aunque parezca que no tengan que ver, voy a
 tener que utilizarlos tarde o temprano. Conceptos como: Complementos a uno y a
 dos, sumadores y su implementación hardware, algoritmos de multiplicación y su
 implementación hardware, cómo funcionan los flags de estado (Carry, Zero,
 Overflow, etc.), cómo se hacen las operaciones aritméticas en CA2, etc.
  
  
  
# 29 de Enero de 2021
 El diseño de la ALU va a depender mucho de los algoritmos que usemos para hacer
 cada operación. Por ejemplo, la multiplicación tiene varios algoritmos y si
 usamos uno u otro, el hardware y la ruta de los datos cambian bastante. 
 También entra en consideración si vamos a tener un multiplicador y/o un
 divisor hardware o no tenerlos.

 Encuentro una implementación de Open-RISC 1000 llamada mor1kx, que tiene un
 repositorio en github. Está todo hecho en verilog, así que tocará aprender a
 usar Verilog.

 ## Nueva lista de TODOs:
  + Aprender Verilog y/o VHDL (preferiblemente Verilog)
  + Buscar un IDE para Verilog (Xilinx isim, modelsim, etc)
  + QFSM (Diseño de autómatas de manera gráfica)

 Dentro de la alu vamos a tener diferentes flags. He visto que tendremos que
 prestar un poco más de atención a un par de ellas que son algo más complejas
 que las demás. La de Zero y la de Overflow (FZ y FO) son las dos menos
 intuitivas de todas.
  
  
  
# 30 de Enero de 2021
 Después de mirar bastante las arquitecturas que puse al principio, creo que lo
 que más me llama la atención es hacer una arquitectura para un procesador
 pequeño, como de microcontrolador. Me atrae la idea de una arquitectura
 Harvard. Desde luego, todo va a ser abierto.
 Partiendo de esto, creo que puedo tomar determinadas decisiones o al menos
 establecer como una base abierta a cambios:
  
  ## Modos de direccionamiento:
    - Indirecto con desplazamiento
    - Registro
    - ¿Inmediato? -> No sé si esto merece la pena
  ### Razones
   Indirecto con desplazamiento porque si el desplazamiento es 0, es usar el
   modo indirecto normal. Además es una solución sencilla que creo que nos
   aporta versatilidad, porque es agnóstica a la organización de la memoria.
   Si usásemos direccionamiento directo/absoluto, tendríamos que especificar la
   dirección de memoria directamente en la instrucción, y me parece que eso
   además enreda en la instrucción, porque queda menos clara y tendríamos que
   dedicar determinados bits de la instrucción para poder direccionar toda la
   memoria.
   El direccionamiento con registro sigue el mismo razonamiento anterior; tener
   los datos en registros es sencillo.
   El direccionamiento inmediato me parece muy útil por su claridad. Además, si
   podemos poner directamente los valores (los inmediatos) nos ahorramos tener
   que cargarlos en registros antes de usarlos.
   

  ## Tipo de almacenamiento de operandos:
    - Registro-Registro (Load-Store)
  ### Razones
   Las instrucciones con la forma **ADD R1, R2** me parecen muy claras. No
   quería utilizar acumulador porque no me parece tan limpio, y utilizar una
   pila hace que las instrucciones se queden muy cortas y que no tengas escritos
   los operandos constantemente.
   Ignoro si hay cuestiones de eficiencia o velocidad entre estas opciones, pero
   mi decisión se basa únicamente en la claridad del formato de la instrucción.

  ## Otras decisiones:
    - En instrucciones de salto, el direccionamiento será indirecto con reg.
  ### Razones
   Podía ser direccionamiento relativo al PC también, pero en mi entendimiento
   es menos versátil porque necesitas saber la dirección de salto en tiempo de
   compilación. Utilizar un registro intermedio para guardar la dirección de
   salto me parece que es una solución sencilla que nos ahorra futuros
   quebraderos de cabeza.
  
  
  
# 1 de Febrero de 2021
 Hago el primer esbozo del juego de instrucciones. Hago por si acaso dos, un
 juego de 25 instrucciones y otro de 12. No sé si es totalmente coherente con
 las decisiones de diseño que he tomado. Por ahora creo que ya el formato de
 mis instrucciones de salto no encaja con el que he puesto en el ISA, porque
 si los saltos van a ser por direccionamiento indirecto con registro, creo que
 el formato de salto **JMP dir** no cumple esa condición.
  
  
  
# 2 de Febrero de 2021
 ¿Cómo afectan el modo de direccionamiento y el tipo de almacenamiento de los
 operandos al formato de las instrucciones?
 |-> Diseño y Evaluación de Arquitectura de Computadores
 |-> Estructura de Computadores
 | Estos libros me sirven como bibliografía para aclarar las dudas
  
  
  
# 5 de Febrero de 2021
 ¿Y si añadimos al juego de 12 instrucciones, una RET y una CALL?
  
  
  
# 6 de Febrero de 2021
 Miro las diferencias entre UC microprogramada y cableada (lógica).
 Las diapositivas de EC del Tema 6 (PDPG) y las de AC del Tema 1 (El 
 computador) son de utilidad.
 ¿Acumulador vs. Registros de Propósito General?
  
  
  
# 9 de Febrero de 2021
 Estudio el pipelining y multiciclo vs. monociclo
  
  
  
# 15 de Febrero de 2021
 Encuentro un proyecto llamado lowrisc/ibex que es una implementación de una 
 CPU de 32 bits hecha por completo en System Verilog. Me podría servir para
 documentar mejor mi trabajo.
 
 Creo que para mi ejemplo particular, una UC micropogramada es mucho trabajo.
  ## ¿Por qué?
  Pienso que con el número reducido de instrucciones que tenemos, una UC
  cableada sigue siendo bastante sencilla. Además, no necesitamos tener la
  escalabilidad en mente, ni vistas a futuro demasiado ambiciosas, por el tipo
  de proyecto que es este.
  
  
  
# 16 de Febrero de 2021
 Realizo parte de la ruta de datos. Entender las tareas de cada isntrucción en
 el nanoMIPS monociclo y las estructuras HW necesarias para su correcto flujo
 me ayuda a hacerme a la idea de una ruta de datos.
  
  
  
# 23 de Febrero de 2021
 Más de una semana haciendo la ruta de datos del nanoMIPS mono y multiciclo.
 Es importante entender cada paso:
  · La extensión de signo de 16 a 32 bits, para las instrucciones de tipo I.
    En las insts LW/SW,el inmediato pasa por la ALU, que opera con números de
    32 bits. Un negativo de 16 bits es positivo cuando se trata como si tuviera
    32, por eso necesiamos un extensor de signo que lo mantenga.
  · En instrucciones BEQ, el inmediato se suma al PC, que es de 32 bits. Luego
    de igual manera se tiene que extender su signo.
  
  ## ¿Zero-Extend?
  Como dirección, `0x1000..000` (16) no es igual a `0x1111...000` (32). Este
  sería el resultado de extender una dirección que empezase por 1. La cosa es
  que las instrucciones no tratan las direcciones como absolutas, sino como
  desplazamientos, **offsets**. Con los offsets sí queremos valores negativos
  para sumárselos al PC, porque así podemos ir a una dirección anterior.
  
  
  
# 24 de Febrero de 2021
 Miro los componentes multiciclo del nanoMIPS, entendiendo por qué necesita
 registros intermedios para cada fase nueva, al ser multiciclo.
  
  
  
# 3 de Marzo de 2021
 Necesitaba repasar conceptos de Tecnologías de Computadores, como lógica
 secuencial, biestables RS, JK, D, y T, tanto síncronos como asíncronos, los
 registros y el registro universal, etc.  
 Todo este tiempo parece estancado, pero será muy útil a futuros porque son
 conceptos que necesito tener bien claros.
  
  
  
# 7 de Marzo de 2021
 Comienzo la ruta de datos propia de mi arquitectura, que sé que va a ser tipo
 Harvard. El resto está todavía pendiente de decisión. Entre las preguntas que
 más me hago, están:  
  · ¿Quito la instrucción MUL y pongo una de RET?  
  · ¿Multiciclo o monociclo?  
        |-> De ser multiciclo, ¿debería ponerle pipeline?  
  · ¿8 ó 16 bits?  
  
 Ben Eaters tiene una playlist donde implementa en hardware un procesador de
 8 bits, que me parece muy didáctico. Debería echarle un ojo.
  
  
  
# 8 de Marzo de 2021
 Cambio el formato de las instrucciones:  
 Instrucciones de longitud fija -> 16 bits  
  
  
  
# 9 de Marzo de 2021
 Vuelvo a cambiar el formato de las instrucciones por uno más coherente  
  -> Quito la instrucción MUL  
  -> Añado una instrucción LDI que carga un inmediato en un registro (porque
     si no, creo que no teníamos forma de guardar valores en memoria ni en
     registro, o sea dependíamos de que en algún lugar estuvieran ya puestos)
  
  
  
# 10 de Marzo de 2021
 Ruta de datos realizada por tipo de instrucción. Es más sencillo así, y luego
 sólo hay que tener en cuenta las "lineas" que llegan al mismo recurso. En esas
 uniones, se pondrá un multiplexor cuyas entradas de control serán las señales
 que saldrán de la UC.
  
  
  
# 11 de Marzo de 2021
 Ruta de datos finalizada por completo.
 Posible cambio del modo de direccionamiento:
  Actualmente las insts tipo I y J son inmediatas. Podríamos implementar 
  direccionamiento indirecto con registro, para poder direccionar más dirs de
  memoria.
  
  
  
# 12 de Marzo de 2021
 Nuevo libro añadido a la bibliografía: "Computational Complexity: A Modern
 Approach", por Sanjeev Arora y Boaz Barak
  
  
  
# 16 de Marzo de 2021
 Componentes en VHDL -> Sumadores y multiplexores
  
  
  
# 17 de Marzo de 2021
 Modelado VHDL: Structural, Behavioral or Dataflow
  
  
    