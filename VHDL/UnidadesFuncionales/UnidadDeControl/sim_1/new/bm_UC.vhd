
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity bm_UC is
end bm_UC;

architecture benchmark_UC of bm_UC is
    component UC
        port (opcode    : in  STD_LOGIC_VECTOR (3 downto 0);
              FZ        : in  STD_LOGIC;
              PCSrc     : out STD_LOGIC;
              RegWrite  : out STD_LOGIC;
              WDSrc0    : out STD_LOGIC;
              WDSrc1    : out STD_LOGIC;
              MemWrite  : out STD_LOGIC;
              RR0Src    : out STD_LOGIC;
              dinSrc    : out STD_LOGIC;
              dirSrc    : out STD_LOGIC;
              ALUOp0    : out STD_LOGIC;
              ALUOp1    : out STD_LOGIC;
              ALUOp2    : out STD_LOGIC;
              ALUEn     : out STD_LOGIC);
    end component;
    
    signal opcode_s : STD_LOGIC_VECTOR (3 downto 0);
    signal FZ_s     : STD_LOGIC;
    signal PCSrc_s, RegWrite_s, WDSrc0_s, WDSrc1_s, MemWrite_s : STD_LOGIC;
    signal RR0Src_s, dinSrc_s, dirSrc_s : STD_LOGIC;
    signal ALUOp0_s, ALUOp1_s, ALUOp2_s : STD_LOGIC;
    signal ALUEn_s                      : STD_LOGIC;
    
begin
    benchmark : UC port map (opcode   => opcode_s,
                             FZ       => FZ_s,
                             PCSrc    => PCSrc_s,
                             RegWrite => RegWrite_s,
                             WDSrc0   => WDSrc0_s,
                             WDSrc1   => WDSrc1_s,
                             MemWrite => MemWrite_s,
                             RR0Src   => RR0Src_s,
                             dinSrc   => dinSrc_s,
                             dirSrc   => dirSrc_s,
                             ALUOp0   => ALUOp0_s,
                             ALUOp1   => ALUOp1_s,
                             ALUOp2   => ALUOp2_s,
                             ALUEn    => ALUEn_s);

    bm_proc : process
        variable tmp : STD_LOGIC_VECTOR (3 downto 0);
        variable aux : STD_LOGIC := '1';
    begin
        FZ_s <= aux;
        for I in 0 to 15 loop
            tmp := STD_LOGIC_VECTOR(TO_UNSIGNED(I, TMP'length));
            opcode_s(3) <= tmp(3);
            opcode_s(2) <= tmp(2);
            opcode_s(1) <= tmp(1);
            opcode_s(0) <= tmp(0);
            wait for 10ns;
        end loop;
        aux := not aux;
    end process;

end benchmark_UC;
