
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity bm_MemInsts is
end bm_MemInsts;

architecture benchmark of bm_MemInsts is
    component MemInsts
        port (din  : in  STD_LOGIC_VECTOR(15 downto 0);
              dir  : in  STD_LOGIC_VECTOR(1 downto 0);
              we   : in  STD_LOGIC;
              clk  : in  STD_LOGIC;
              dout : out STD_LOGIC_VECTOR(15 downto 0));
    end component;

    signal din_s, dout_s : STD_LOGIC_VECTOR (15 downto 0);
    signal dir_s         : STD_LOGIC_VECTOR (1 downto 0);
    signal we_s          : STD_LOGIC;
    signal clk_s         : STD_LOGIC := '1';

    constant T : time := 10ns;

begin

    benchmark : MemInsts
        port map (din  => din_s,
                  dir  => dir_s,
                  we   => we_s,
                  clk  => clk_s,
                  dout => dout_s);


    clk_proc : process
    begin
        clk_s <= '0';
            wait for T/2;
        clk_s <= '1';
            wait for T/2;
    end process;


    bm_proc : process
    begin
        -- Fill memory in
        we_s  <= '1';
        dir_s <= "00";
            din_s <= "0000000000000000";
            wait for T;
        dir_s <= "01";
            din_s <= "0000000000000001";
            wait for T;
        dir_s <= "10";
            din_s <= "0000000000000010";
            wait for T;
        dir_s <= "11";
            din_s <= "0000000000000011";
            wait for T;

        -- Read Memory
        we_s <= '0';

        for K in 0 to (2 ** dir_s'length) - 1 loop
            dir_s <= STD_LOGIC_VECTOR(TO_UNSIGNED(K, dir_s'length));
            wait for T;
        end loop;

    end process;

end benchmark;
