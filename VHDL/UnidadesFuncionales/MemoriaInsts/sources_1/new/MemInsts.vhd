
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity MemInsts is
    port (din  : in  STD_LOGIC_VECTOR(15 downto 0);
          dir  : in  STD_LOGIC_VECTOR(1 downto 0);
          we   : in  STD_LOGIC;
          clk  : in  STD_LOGIC;
          dout : out STD_LOGIC_VECTOR(15 downto 0));
end MemInsts;

architecture Arch_MemInsts of MemInsts is
    component Decod2_4
        port (e  : in  STD_LOGIC_VECTOR (1 downto 0);
              en : in  STD_LOGIC;
              S  : out STD_LOGIC_VECTOR (3 downto 0));
    end component;

    component GenericReg
        generic (DataWidth : integer := 8);
        port (en      : in  STD_LOGIC;
              dataIn  : in  STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
              dataOut : out STD_LOGIC_VECTOR (DataWidth - 1 downto 0));
    end component;

    component GenericMux4_2
        generic (DataWidth : integer := 8);
        port (a, b, c, d : in  STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
              Z          : in  STD_LOGIC_VECTOR (1 downto 0);
              S          : out STD_LOGIC_VECTOR (DataWidth - 1 downto 0));
    end component;

    type v4 is array (natural range <>) of STD_LOGIC_VECTOR(15 downto 0);
    signal regOut : v4 (3 downto 0);

    constant DW : integer := 16;

    signal decods_s : STD_LOGIC_VECTOR (3 downto 0);
    signal aux_s    : STD_LOGIC_VECTOR (3 downto 0);

begin

    decod : Decod2_4
        port map (e  => dir,
                  en => we,
                  s  => decods_s);

    gen_regs : for I in 0 to 3 generate
    begin
        aux_s(I) <= decods_s(I) and Clk;
        reg_x : GenericReg
            generic map (DataWidth => DW)
            port map (dataIn  => din,
                      en      => aux_s(I),
                      dataOut => regOut(I));
    end generate gen_regs;

    mux : GenericMux4_2
        generic map (DataWidth => DW)
        port map (a => regOut(0),
                  b => regOut(1),
                  c => regOut(2),
                  d => regOut(3),
                  Z => dir,
                  S => dout);

end Arch_MemInsts;