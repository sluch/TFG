-- Memoria de 256 posiciones de una anchura generica

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity GenericMem is
    generic (DataWidth : integer := 8);
    port (clk  : in  STD_LOGIC;
          we   : in  STD_LOGIC;
          dir  : in  STD_LOGIC_VECTOR (7 downto 0);
          din  : in  STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
          dout : out STD_LOGIC_VECTOR (DataWidth - 1 downto 0));
end GenericMem;

architecture Arch_GenericMem of GenericMem is
    type ram_type is array (255 downto 0) of STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
    signal RAM : ram_type;
begin
    process (clk)
    begin
        if (rising_edge(clk)) then
            if (we = '1') then
                RAM(conv_integer(dir)) <= din;
            end if;
        end if;
    end process;

    dout <= RAM(conv_integer(dir));

end Arch_GenericMem;
