
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity bm_GenericMem is
end bm_GenericMem;

architecture benchmark of bm_GenericMem is
    component GenericMem
        generic (DataWidth : integer := 8);
        port (clk  : in  STD_LOGIC;
              we   : in  STD_LOGIC;
              dir  : in  STD_LOGIC_VECTOR (7 downto 0);
              din  : in  STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
              dout : out STD_LOGIC_VECTOR (DataWidth - 1 downto 0));
    end component;
    
    constant DW : integer := 16;
    constant T  : time := 1ns;

    signal we_s          : STD_LOGIC;
    signal clk_s         : STD_LOGIC := '1';
    signal din_s, dout_s : STD_LOGIC_VECTOR (DW - 1 downto 0) := (others => '0');
    signal dir_s         : STD_LOGIC_VECTOR (7 downto 0);

begin

    benchmark : GenericMem
        generic map (DataWidth => DW)
        port map (din  => din_s,
                  dir  => dir_s,
                  we   => we_s,
                  clk  => clk_s,
                  dout => dout_s);

    clk_proc : process
    begin
        clk_s <= '0';
            wait for T/2;
        clk_s <= '1';
            wait for T/2;
    end process;

    bm_proc : process
    begin
        -- write
        
        we_s  <= '1';
        din_s <= "1101101101101101"; -- DB6D
        for I in 0 to 255 loop
            dir_s <= STD_LOGIC_VECTOR(TO_UNSIGNED(I, dir_s'length));
            wait for T;
        end loop;
        
        dir_s <= "00000000";
        din_s <= "0000000000001111"; -- 000F
            wait for T;

        dir_s <= "00000001";
        din_s <= "0000000011110000"; -- 00F0
            wait for T;

        dir_s <= "00000010";
        din_s <= "0000111100000000"; -- 0F00
            wait for T;

        dir_s <= "00000011";
        din_s <= "1111000000000000"; --F000
            wait for T;

        dir_s <= "11111111";
        din_s <= "1111111111111111"; -- FFFF
            wait for T;

        -- read
        we_s  <= '0';
        dir_s <= "00000000";
        for K in 0 to 255 loop
            we_s  <= '0';
            dir_s <= STD_LOGIC_VECTOR(TO_UNSIGNED(K, dir_s'length));
            wait for T;
        end loop;
    end process;

end benchmark;
