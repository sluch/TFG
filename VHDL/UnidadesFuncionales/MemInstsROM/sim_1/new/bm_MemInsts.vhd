library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity bm_MemInsts is
end bm_MemInsts;

architecture benchmark of bm_MemInsts is
    component MemInsts
        port (dir  : in  STD_LOGIC_VECTOR (7 downto 0);
              dout : out STD_LOGIC_VECTOR (15 downto 0));
    end component;

    signal dir_s  : STD_LOGIC_VECTOR (7 downto 0);
    signal dout_s : STD_LOGIC_VECTOR (15 downto 0);

    constant T : time := 2ns;

begin

    benchmark : MemInsts
        port map (dir  => dir_s,
                  dout => dout_s);

    bm_proc : process
    begin
        for I in 0 to 255 loop
            dir_s <= STD_LOGIC_VECTOR(TO_UNSIGNED(I, dir_s'length));
            wait for T;
        end loop;
    end process;

end benchmark;
