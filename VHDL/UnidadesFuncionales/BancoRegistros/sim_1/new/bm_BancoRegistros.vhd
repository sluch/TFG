
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity bm_BancoRegistros is
end bm_BancoRegistros;

architecture benchmark of bm_BancoRegistros is
    component BancoRegistros
    port (ReadReg0 : in  STD_LOGIC_VECTOR (2 downto 0);
          ReadReg1 : in  STD_LOGIC_VECTOR (2 downto 0);
          RegSel   : in  STD_LOGIC_VECTOR (2 downto 0);
          WriteDat : in  STD_LOGIC_VECTOR (7 downto 0);
          WriteEn  : in  STD_LOGIC;
          ReadDat0 : out STD_LOGIC_VECTOR (7 downto 0);
          ReadDat1 : out STD_LOGIC_VECTOR (7 downto 0));
    end component;

    signal RR0_s, RR1_s, RS_s : STD_LOGIC_VECTOR (2 downto 0);
    signal WD_s, RD0_s, RD1_s : STD_LOGIC_VECTOR (7 downto 0);
    signal WE_s               : STD_LOGIC;

begin

    benchmark : BancoRegistros
        port map (ReadReg0 => RR0_s,
                  ReadReg1 => RR1_s,
                  RegSel   => RS_s,
                  WriteDat => WD_s,
                  WriteEn  => WE_s,
                  ReadDat0 => RD0_s,
                  ReadDat1 => RD1_s);

    bm_proc : process
    begin
        -- Fill registers in
        RR0_s <= "001";
        RR1_s <= "010";
        WE_s  <= '1';

        for I in 0 to 7 loop
            RS_s <= STD_LOGIC_VECTOR(TO_UNSIGNED(I, RS_s'length));
            WD_s <= STD_LOGIC_VECTOR(TO_UNSIGNED(I, WD_s'length));
            wait for 10ns;
        end loop;

        WE_s  <= '0';
        
        for I in 0 to 7 loop
            RR0_s <= STD_LOGIC_VECTOR(TO_UNSIGNED(I, RR0_s'length));
            RR1_s <= STD_LOGIC_VECTOR(TO_UNSIGNED(I, RR1_s'length));
            wait for 10ns;
        end loop;

    end process;

end benchmark;
