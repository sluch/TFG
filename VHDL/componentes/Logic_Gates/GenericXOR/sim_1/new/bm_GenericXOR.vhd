
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity bm_GenericOR is
end bm_GenericOR;

architecture benchmark of bm_GenericOR is
    component GenericXOR
        generic (DataWidth : integer);
        port (a, b : in  STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
              S    : out STD_LOGIC_VECTOR (DataWidth - 1 downto 0));
    end component;

    constant DW : integer := 4;

    signal a_s, b_s : STD_LOGIC_VECTOR (DW - 1 downto 0);
    signal S_s      : STD_LOGIC_VECTOR (DW - 1 downto 0);

begin

    benchmark : GenericXOR
    generic map (DataWidth => DW)
    port map (a => a_s,
              b => b_s,
              s => s_s);

    bm_proc : process
    begin
        for I in 0 to (2 ** a_s'length) - 1 loop
            for J in 0 to (2 ** b_s'length) - 1 loop
                a_s <= STD_LOGIC_VECTOR(to_unsigned(I, a_s'length));
                b_s <= STD_LOGIC_VECTOR(to_unsigned(J, b_s'length));
                wait for 2ns;
            end loop;
        end loop;
    end process;


end benchmark;

