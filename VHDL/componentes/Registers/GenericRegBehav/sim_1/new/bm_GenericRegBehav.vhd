
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity bm_GenericRegBehav is
end bm_GenericRegBehav;

architecture benchmark of bm_GenericRegBehav is
    component GenericRegBehav
        generic (DataWidth : integer := 8);
        port (en      : in  STD_LOGIC;
              dataIn  : in  STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
              dataOut : out STD_LOGIC_VECTOR (DataWidth - 1 downto 0) := (others=>'0'));
    end component;
    
    constant DW : integer := 8;
    
    signal en_s : STD_LOGIC;
    signal din_s, dout_s : STD_LOGIC_VECTOR (DW - 1 downto 0);
    
begin
    benchmark : GenericRegBehav
        generic map (DataWidth => DW)
        port map (en      => en_s,
                  dataIn  => din_s,
                  dataOut => dout_s);
                  
    clk_proc : process
    begin  
        en_s <= '0';
            wait for 1ns;
        en_s <= '1';
            wait for 1ns;
    end process;                  

    bm_proc : process
        variable tmp : STD_LOGIC_VECTOR (DW downto 0);
    begin
        wait for 10ns;
        for I in 0 to (2 ** DW) - 1 loop
            din_s <= STD_LOGIC_VECTOR(to_unsigned(I, DW));
            wait for 2ns;
        end loop;
    end process;

end benchmark;
