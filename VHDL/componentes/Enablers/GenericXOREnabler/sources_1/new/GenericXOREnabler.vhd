
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity GenericXOREnabler is
    generic (DataWidth : integer := 8);
    port (a : in  STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
          Z : in  STD_LOGIC;
          S : out STD_LOGIC_VECTOR (DataWidth - 1 downto 0));
end GenericXOREnabler;

architecture Behavioral of GenericXOREnabler is
begin
    gen : for I in DataWidth -1 downto 0 generate
        S(I) <= (Z xor a(I));
    end generate;


end Behavioral;
