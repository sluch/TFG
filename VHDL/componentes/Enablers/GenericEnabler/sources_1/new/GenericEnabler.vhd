
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity GenericEnabler is
    generic (DataWidth : integer := 8);
    port (a : in  STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
          Z : in  STD_LOGIC;
          S : out STD_LOGIC_VECTOR (DataWidth - 1 downto 0));
end GenericEnabler;

architecture Arch_GenericEnabler of GenericEnabler is
begin

    gen : for I in DataWidth -1 downto 0 generate
        S(I) <= (Z and a(I));
    end generate;

end Arch_GenericEnabler;
