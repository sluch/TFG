
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity bm_Decod4_16 is
end bm_Decod4_16;

architecture benchmark_Decod4_16 of bm_Decod4_16 is

    component Decod4_16
        port (e  : in  STD_LOGIC_VECTOR (3 downto 0);
              en : in  STD_LOGIC;
              S  : out STD_LOGIC_VECTOR (15 downto 0));
    end component;
    
    signal e_s  : STD_LOGIC_VECTOR (3 downto 0);
    signal en_s : STD_LOGIC;
    signal S_s  : STD_LOGIC_VECTOR (15 downto 0);

begin

    benchmark : Decod4_16 port map(en => en_s,
                                   e  => e_s,
                                   S  => S_s);
         
         
    bm_proc : process
        variable TMP   : STD_LOGIC_VECTOR(0 to 3);
    begin
        en_s <= '1';
        for I in 0 to (2 ** e_s'length) -1 loop
            TMP := STD_LOGIC_VECTOR(TO_UNSIGNED(I, TMP'length));
            e_s <= TMP;
            wait for 10ns;
        end loop;
        
        en_s <= '0';
        for I in 0 to (2 ** e_s'length) - 1 loop
            TMP := STD_LOGIC_VECTOR(TO_UNSIGNED(I, TMP'length));
            e_s <= TMP;
            wait for 10ns;
        end loop;
    end process;
    
end benchmark_Decod4_16;
