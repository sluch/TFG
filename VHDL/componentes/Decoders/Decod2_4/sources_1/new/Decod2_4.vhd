
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Decod2_4 is
    port (e  : in  STD_LOGIC_VECTOR (1 downto 0);
          en : in  STD_LOGIC;
          S  : out STD_LOGIC_VECTOR (3 downto 0));
end Decod2_4;

architecture Arch_Decod2_4 of Decod2_4 is
begin
    S(0) <= (en) and ((not e(1)) and (not e(0))); -- 0 0
    S(1) <= (en) and ((not e(1)) and     (e(0))); -- 0 1
    S(2) <= (en) and     ((e(1)) and (not e(0))); -- 1 0
    S(3) <= (en) and     ((e(1)) and     (e(0))); -- 1 1
end Arch_Decod2_4;


