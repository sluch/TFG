
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Adder4bit is
    port (a, b : in  STD_LOGIC_VECTOR (3 downto 0);
          Cin  : in  STD_LOGIC;
          S    : out STD_LOGIC_VECTOR (3 downto 0);
          Cout : out STD_LOGIC);
end Adder4bit;

architecture Arch_Adder4bit of Adder4bit is
    component FullAdder
        port (a, b, Cin : in  STD_LOGIC;
              Cout, S   : out STD_LOGIC);
    end component;
    
    signal Cout0_s, Cout1_s, Cout2_s : STD_LOGIC;
    
begin
    w0 : FullAdder port map (a    => a(0),
                             b    => b(0),
                             Cin  => Cin,
                             Cout => Cout0_s,
                             S    => S(0));
                             
    -- Para w1 y w2 se podr�a hacer un loop          
    w1 : FullAdder port map (a    => a(1),
                             b    => b(1),
                             Cin  => Cout0_s,
                             Cout => Cout1_s,
                             S    => S(1));
                            
    w2 : FullAdder port map (a    => a(2),
                             b    => b(2),
                             Cin  => Cout1_s,
                             Cout => Cout2_s,
                             S    => S(2));
                             
    w3 : FullAdder port map (a    => a(3),
                             b    => b(3),
                             Cin  => Cout2_s,
                             Cout => Cout,
                             S    => S(3));

end Arch_Adder4bit;
