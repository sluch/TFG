
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity FullAdder is
    port (a, b, Cin : in  STD_LOGIC;
          Cout, S   : out STD_LOGIC);
end FullAdder;

architecture Arch_FullAdder of FullAdder is
begin
    Cout <= (a and b) or (a and Cin) or (b and Cin);
    S    <= a xor b xor Cin;
end Arch_FullAdder;
