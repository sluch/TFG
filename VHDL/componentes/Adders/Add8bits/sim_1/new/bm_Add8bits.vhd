
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL; -- to_unsigned()

entity bm_Adder8bit is
end bm_Adder8bit;

architecture benchmark of bm_Adder8bit is
    component Adder8bit
        port (a, b : in  STD_LOGIC_VECTOR (7 downto 0);
              Cin  : in  STD_LOGIC;
              S    : out STD_LOGIC_VECTOR (7 downto 0);
              Cout : out STD_LOGIC);
    end component;
    
    signal a_s, b_s, S_s   : STD_LOGIC_VECTOR (7 downto 0);
    signal Cin_s, Cout_s   : STD_LOGIC;
    
begin
    benchmark : Adder8bit port map (a    => a_s,
                                    b    => b_s,
                                    S    => S_s,
                                    Cin  => Cin_s,
                                    Cout => Cout_s);
                                    
    bm_proc : process
    begin

        -- 0 - 0
        Cin_s <= '1';
        b_s   <= "11111111";
        a_s   <= "00000000";
        wait for 2ns;




         Cin_s <= '0';
         for I in 0 to (2 ** a_s'length) - 1 loop
             for J in 0 to (2 ** b_s'length) - 1 loop
                     a_s <= STD_LOGIC_VECTOR(to_unsigned(I, a_s'length));
                     b_s <= STD_LOGIC_VECTOR(to_unsigned(J, b_s'length));
                     wait for 2ns;
                 end loop;
             end loop;
    end process;

end benchmark;
