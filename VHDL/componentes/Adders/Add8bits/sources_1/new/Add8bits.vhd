
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Adder8bit is
    port (a, b : in  STD_LOGIC_VECTOR (7 downto 0);
          Cin  : in  STD_LOGIC;
          S    : out STD_LOGIC_VECTOR (7 downto 0);
          Cout : out STD_LOGIC);
end Adder8bit;

architecture Arch_Adder8bit of Adder8bit is
    component Adder4bit
        port (a, b : in  STD_LOGIC_VECTOR (3 downto 0);
              Cin  : in  STD_LOGIC;
              S    : out STD_LOGIC_VECTOR (3 downto 0);
              Cout : out STD_LOGIC);
    end component;
    
    signal Cout0_s : STD_LOGIC;
    
begin
    w0 : Adder4bit port map (a    => a(3 downto 0),
                             b    => b(3 downto 0),
                             Cin  => Cin,
                             Cout => Cout0_s,
                             S    => S(3 downto 0));
                             
    w1 : Adder4bit port map (a    => a(7 downto 4),
                             b    => b(7 downto 4),
                             Cin  => Cout0_s,
                             Cout => Cout,
                             S    => S(7 downto 4));

end Arch_Adder8bit;
