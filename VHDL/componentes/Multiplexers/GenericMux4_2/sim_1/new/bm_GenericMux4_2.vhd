
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity bm_GenericMux4_2 is
end bm_GenericMux4_2;

architecture benchmark of bm_GenericMux4_2 is
    component GenericMux4_2
        generic (DataWidth : integer);
        port (a, b, c, d : in  STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
              Z          : in  STD_LOGIC_VECTOR (1 downto 0);
              S          : out STD_LOGIC_VECTOR (DataWidth - 1 downto 0));
    end component;

    constant DW : integer := 4;

    signal a_s, b_s, c_s, d_s, S_s : STD_LOGIC_VECTOR (DW - 1 downto 0);
    signal Z_s                     : STD_LOGIC_VECTOR (1 downto 0);

begin
    benchmark : GenericMux4_2
    generic map (DataWidth => DW)
    port map (a => a_s,
              b => b_s,
              c => c_s,
              d => d_s,
              Z => Z_s,
              S => S_s);

    bm_proc : process
    begin
        a_s <= "1010";
        b_s <= "1011";
        c_s <= "1100";
        d_s <= "1101";
        for M in STD_LOGIC range '0' to '1' loop
            for N in STD_LOGIC range '0' to '1' loop
                Z_s(1) <= M;
                Z_s(0) <= N;
                wait for 2ns;
            end loop;
        end loop;
    end process;

end benchmark;
