
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity GenericMux2_1 is
    generic (DataWidth : integer := 8);
    port (a, b : in  STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
          Z    : in  STD_LOGIC;
          S    : out STD_LOGIC_VECTOR (DataWidth - 1 downto 0));
end GenericMux2_1;

architecture Arch_GenericMux2_1 of GenericMux2_1 is
    component GenericEnabler
        generic (DataWidth : integer := 8);
        port (a : in  STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
              Z : in  STD_LOGIC;
              S : out STD_LOGIC_VECTOR (DataWidth - 1 downto 0));
    end component;

    component GenericOR
        generic (DataWidth : integer := 8);
        port (a, b : in  STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
              S    : out STD_LOGIC_VECTOR (DataWidth - 1 downto 0));
    end component;



    signal en1out_s, en2out_s : STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
    signal notZ : STD_LOGIC;

begin

    notZ <= not Z;

    w0 : GenericEnabler
    generic map (DataWidth => DataWidth)
    port map (a => a,
              Z => notZ,
              S => en1out_s);

    w1 : GenericEnabler
    generic map (DataWidth => DataWidth)
    port map (a => b,
              Z => Z,
              S => en2out_s);

    w3 : GenericOR
    generic map (DataWidth => DataWidth)
    port map(a => en1out_s,
             b => en2out_s,
             S => S);

end Arch_GenericMux2_1;
