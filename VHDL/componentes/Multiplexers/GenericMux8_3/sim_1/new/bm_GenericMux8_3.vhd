
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity bm_GenericMux8_3 is
end bm_GenericMux8_3;

architecture benchmark of bm_GenericMux8_3 is
    component GenericMux8_3
        generic (DataWidth : integer := 8);
        port (a, b, c, d : in  STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
              e, f, g, h : in  STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
              Z          : in  STD_LOGIC_VECTOR (2 downto 0);
              S          : out STD_LOGIC_VECTOR (DataWidth - 1 downto 0));
    end component;

    constant DW : integer := 8;
    signal a_s, b_s, c_s, d_s : STD_LOGIC_VECTOR (DW - 1 downto 0);
    signal e_s, f_s, g_s, h_s : STD_LOGIC_VECTOR (DW - 1 downto 0);
    signal Z_s                : STD_LOGIC_VECTOR (2 downto 0);
    signal S_s                : STD_LOGIC_VECTOR (DW - 1 downto 0);

begin
    benchmark : GenericMux8_3
        generic map (DataWidth => DW)
        port map (a => a_s,
                  b => b_s,
                  c => c_s,
                  d => d_s,
                  e => e_s,
                  f => f_s,
                  g => g_s,
                  h => h_s,
                  Z => Z_s,
                  S => S_s);

    bm_proc : process
    begin
        a_s <= "00001010";
        b_s <= "00001011";
        c_s <= "00001100";
        d_s <= "00001101";
        e_s <= "00001110";
        f_s <= "00001111";
        g_s <= "10011001";
        h_s <= "01100110";

        for I in 0 to 7 loop
            Z_s <= STD_LOGIC_VECTOR(TO_UNSIGNED(I, Z_s'length));
            wait for 2ns;
        end loop;
    end process;


end benchmark;
