library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity bm_IntegracionMI is
end bm_IntegracionMI;

architecture benchmark of bm_IntegracionMI is
    component IntegracionMI
        port (clk    : in  STD_LOGIC;
              FZ     : in  STD_LOGIC;
              ALURes : out STD_LOGIC_VECTOR (7 downto 0));
    end component;

    constant T            : time := 2 ns;
    signal   clk_s        : STD_LOGIC;
    signal   FZ_s         : STD_LOGIC;
    signal   ALURes_s : STD_LOGIC_VECTOR (7 downto 0);
    
begin
    
    bm : IntegracionMi
        port map (clk => clk_s,
                  FZ  => FZ_s,
                  ALUres => ALURes_s);
                  

    FZ_s <= '0';

    clk_proc : process
    begin
        clk_s <= '0';
            wait for T/2;
        clk_s <= '1';
            wait for T/2;
    end process;
    
end benchmark;
