library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity IntegracionMI is
    port (clk    : in  STD_LOGIC;
          FZ     : in  STD_LOGIC;
          ALURes : out STD_LOGIC_VECTOR (7 downto 0));
end IntegracionMI;

architecture rtl of IntegracionMI is
    component GenericMux2_1
        generic (DataWidth : integer);
        port (a, b : in  STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
              Z    : in  STD_LOGIC;
              S    : out STD_LOGIC_VECTOR (DataWidth - 1 downto 0));
    end component;

    component Adder8bit
        port (a, b : in  STD_LOGIC_VECTOR (7 downto 0);
              Cin  : in  STD_LOGIC;
              S    : out STD_LOGIC_VECTOR (7 downto 0);
              Cout : out STD_LOGIC);
    end component;

    component GenericRegBehav
        generic (DataWidth : integer := 8);
        port (en      : in  STD_LOGIC;
              dataIn  : in  STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
              dataOut : out STD_LOGIC_VECTOR (DataWidth - 1 downto 0));
    end component;

    component MemInsts
        port (clk  : in  STD_LOGIC;
              dir  : in  STD_LOGIC_VECTOR (7 downto 0);
              dout : out STD_LOGIC_VECTOR (15 downto 0));
    end component;

    component UC
        port (opcode    : in  STD_LOGIC_VECTOR (3 downto 0);
              FZ        : in  STD_LOGIC;
              PCSrc     : out STD_LOGIC;
              RegWrite  : out STD_LOGIC;
              WDSrc0    : out STD_LOGIC;
              WDSrc1    : out STD_LOGIC;
              MemWrite  : out STD_LOGIC;
              RR0Src    : out STD_LOGIC;
              dinSrc    : out STD_LOGIC;
              dirSrc    : out STD_LOGIC;
              ALUOp0    : out STD_LOGIC;
              ALUOp1    : out STD_LOGIC;
              ALUOp2    : out STD_LOGIC;
              ALUEn     : out STD_LOGIC);
    end component;

    component BancoRegistrosBehav
        port (--clk      : in  STD_LOGIC;
              ReadReg0 : in  STD_LOGIC_VECTOR (2 downto 0);
              ReadReg1 : in  STD_LOGIC_VECTOR (2 downto 0);
              RegSel   : in  STD_LOGIC_VECTOR (2 downto 0);
              WriteDat : in  STD_LOGIC_VECTOR (7 downto 0);
              WriteEn  : in  STD_LOGIC;
              ReadDat0 : out STD_LOGIC_VECTOR (7 downto 0);
              ReadDat1 : out STD_LOGIC_VECTOR (7 downto 0));
    end component;

    component GenericMux4_2
        generic (DataWidth : integer := 8);
        port (a, b, c, d : in  STD_LOGIC_VECTOR (DataWidth - 1 downto 0);
              Z          : in  STD_LOGIC_VECTOR (1 downto 0);
              S          : out STD_LOGIC_VECTOR (DataWidth - 1 downto 0));
    end component;

    component UAL
        port (A, B        : in  STD_LOGIC_VECTOR (7 downto 0);
              ALUOp2      : in  STD_LOGIC;
              ALUOp1      : in  STD_LOGIC;
              ALUOp0      : in  STD_LOGIC;
              Res         : out STD_LOGIC_VECTOR (7 downto 0);
              ZF, OFF, PF : out STD_LOGIC;
              SF, CF      : out STD_LOGIC);
    end component;

    constant DW   : integer := 8;
    constant DW2  : integer := 3;

    -- Signals intermedias
    signal PCin_s   : STD_LOGIC_VECTOR (7 downto 0);
    signal PCOut_s  : STD_LOGIC_VECTOR (7 downto 0);
    signal AddOut_s : STD_LOGIC_VECTOR (7 downto 0);
    signal MIOut_s  : STD_LOGIC_VECTOR (15 downto 0);
    signal JMPDir_s : STD_LOGIC_VECTOR (7 downto 0);
    signal InmI_s   : STD_LOGIC_VECTOR (7 downto 0);
    signal opcode_s : STD_LOGIC_VECTOR (3 downto 0);
    signal RR0in_s  : STD_LOGIC_VECTOR (2 downto 0);
    signal RD0_s    : STD_LOGIC_VECTOR (7 downto 0);
    signal RD1_s    : STD_LOGIC_VECTOR (7 downto 0);
    signal WDat_s   : STD_LOGIC_VECTOR (7 downto 0);
    signal ALURes_s : STD_LOGIC_VECTOR (7 downto 0);

    signal Rr_s, Rd_s, Rs_s : STD_LOGIC_VECTOR (2 downto 0);

    signal PCSrc_s   : STD_LOGIC;
    signal WriteEn_s : STD_LOGIC;
    signal RR0Src_s  : STD_LOGIC;
    signal WDSrc1_s  : STD_LOGIC;
    signal WDSrc0_s  : STD_LOGIC;
    signal ALUOp2_s  : STD_LOGIC;
    signal ALUOp1_s  : STD_LOGIC;
    signal ALUOp0_s  : STD_LOGIC;
    signal ALUEn_s   : STD_LOGIC;
    
    signal ZF_s      : STD_LOGIC;
    signal FlagZ_s   : STD_LOGIC;


begin

    m0 : GenericMux2_1
        generic map (DataWidth => DW)
        port map (a => AddOut_s,
                  b => JMPDir_s,
                  Z => PCSrc_s,
                  S => PCin_s);

    PC : GenericRegBehav
        generic map (DataWidth => DW)
        port map (en      => clk,
                  dataIn  => PCin_s,
                  dataOut => PCOut_s);

    A0 : Adder8bit
        port map (a    => "00000001",
                  b    => PCOut_s,
                  Cin  => '0',
                  S    => AddOut_s,
                  Cout => open);

    MI : MemInsts
        port map (clk  => clk,
                  dir  => PCOut_s,
                  dout => MIOut_s);

    JMPDir_s <= MIOut_s (7 downto 0);
    InmI_s   <= MIOut_s (7 downto 0);
    opcode_s <= MIOut_s (15 downto 12);
    Rd_s     <= MIOut_s (11 downto 9);
    Rr_s     <= MIOut_s (8 downto 6);
    Rs_s     <= MIOut_s (5 downto 3);



    CU : UC
        port map (opcode   => opcode_s,
                  FZ       => FlagZ_s,
                  PCSrc    => PCSrc_s,
                  RegWrite => WriteEn_s,
                  WDSrc1   => WDSrc1_s,
                  WDSrc0   => WDSrc0_s,
                  MemWrite => open,
                  RR0Src   => RR0Src_s,
                  dinSrc   => open,
                  dirSrc   => open,
                  ALUOp0   => ALUOp0_s,
                  ALUOp1   => ALUOp1_s,
                  ALUOp2   => ALUOp2_s,
                  ALUen    => ALUEn_s);

    m1 : GenericMux2_1
        generic map (DataWidth => DW2)
        port map (a => Rr_s,
                  b => Rd_s,
                  Z => RR0Src_s,
                  S => RR0in_s);

    BdR : BancoRegistrosBehav
        port map (--clk      => clk,
                  ReadReg0 => RR0in_s,
                  ReadReg1 => Rs_s,
                  RegSel   => Rd_s,
                  WriteDat => WDat_s,
                  WriteEn  => WriteEn_s,
                  ReadDat0 => RD0_s,
                  ReadDat1 => RD1_s);

    m2 : GenericMux4_2
        generic map (DataWidth => DW)
        port map (a    => ALURes_s,
                  b    => "10111011",
                  c    => RD0_s,
                  d    => InmI_s,
                  Z(1) => WDSrc1_s,
                  Z(0) => WDSrc0_s,
                  S    => WDat_s);

    ALU : UAL
        port map(A => RD0_s,
                 B => RD1_s,
                 ALUop2 => ALUOp2_s,
                 ALUOp1 => ALUOp1_s,
                 ALUOp0 => ALUOp0_s,
                 Res    => ALURes_s,
                 ZF     => ZF_s,
                 OFF    => open,
                 PF     => open,
                 SF     => open,
                 CF     => open);

    ALURes <= ALURes_s;

    delayFZ : process(clk)
    begin
        if rising_edge(clk) then
            FlagZ_s <= ZF_s;
        end if;
    end process;


    

end rtl;