# TFG
 Repositorio de mi TFG, el diseño de una arquitectura de computador.

 ![Ruta de Datos](/images/DataPath/RutaDeDatos.png)



# Bibliografía usada:
 · Diseño y evaluación de arquitecturas de computadores (Marta Beltrán Pardo y Antonio Guzmán Sacristán)  
 · Estructura de Computadores (Miquel Albert Orenga y Gerard Enrique Manonellas)
